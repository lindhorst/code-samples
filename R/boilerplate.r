## commands useful to run at the beginning of any script

## remove objects
rm(list=ls())

## remove any loaded libraries
if(!is.null(names(sessionInfo()$otherPkgs))) {
    invisible(lapply(paste0('package:', names(sessionInfo()$otherPkgs)),
                     detach, character.only=TRUE, unload=TRUE))
}

## change printing width on terminal
options(width=400)

## change the number of line printed, default is 99999
options(max.print=100)  # print less
options(max.print=.Machine$integer.max)  # print as much as possible



