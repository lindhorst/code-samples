## prepare data and labels
mydata = data.frame(varA=c(2,4,6,8,10), varB = c("aa","bb","dd","ee","ff"),
                    varC=c(TRUE, TRUE, FALSE, TRUE, FALSE))
 
mylabs = c("My label for A", "Label for B", "Label for variable C")
mylabs = as.list(mylabs)
names(mylabs) = names(mydata)
 
## label the data frame
library(Hmisc)
label(mydata) = mylabs
 
## export in SAS format
library(SASxport)
write.xport(mydata, file="myout.dat")
