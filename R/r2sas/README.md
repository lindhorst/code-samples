# Export an R data frame for SAS

## Introduction

This example shows how to export a simple data frame from R in a format useful
for reading into SAS. It also demonstrates how to add labels to each column of
the data frame.

## Prerequisites

The R package needed to exchange data with SAS is called 'SASxport'. Install it
with:

```R
install.packages('SASxport')
```

Package 'Hmisc' is also needed, specifically for labeling the variables. It is a
prerequisite for 'SASxport' and does not need to be installed separately.

## Try the example

Open an R session and run the code in file ```r2sas.r``` interactively.

