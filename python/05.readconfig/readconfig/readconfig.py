import configparser
import sys
import os

def readconfig(inistring = None, inifiles = []):
    """Read and parse configuration options from a string and/or from files.

    The string and files are in Microsoft "ini" format.

    There is a priority in which option values are assigned. 'inistring' has the
    lowest priority. Files in the list are ordered in increasing prority. This
    is a result of the way the configuration object is built. The contents if
    ini values is added first from the string, then from the files in list
    order. As key/value pairs are read in, any existing keys are overwritten.

    The return value of the function in a 2 element tuple. The first is an exit
    code, with 0 indicating success. The second is a dictionary with the
    contents of the ini structure. An ini input of the form:

    [DEFAULT]
    def-1 = def-val-1
    def-2 = def-val-2

    [section-1]
    key-1 = val-1
    key-2 = val-2

    [section-2]
    key-3 = val-3
    key-4 = val-4

    will result in the dictionary:

    {
      'DEFAULT': {
        'def-1': 'def-val-1',
        'def-2': 'def-val-2'},
      'section-1': {
        'def-1': 'def-val-1',
        'def-2': 'def-val-2',
        'key-1': 'val-1',
        'key-2': 'val-2'},
      'section-2': {
        'def-1': 'def-val-1',
        'def-2': 'def-val-2',
        'key-3': 'val-3',
        'key-4': 'val-4'
      }
    }
    """

    exit_code = 0
    
    cfg = configparser.ConfigParser()
    if inistring is not None:
        try:
            cfg.read_string(inistring)
        except Exception as e:
            sys.stderr.write(f"ERROR:\n{str(e)}\n")
            exit_code = 1

    for fil in inifiles:
        cfg_fn = os.path.expanduser(fil)
        try:
            cfg_fh = open(cfg_fn, 'r')
            cfg.read_file(cfg_fh)
            cfg_fh.close()
        except Exception as e:
            sys.stderr.write(f"ERROR:\n{str(e)}\n")
            exit_code = 1

    return (exit_code, {x: dict(cfg[x]) for x in cfg})
