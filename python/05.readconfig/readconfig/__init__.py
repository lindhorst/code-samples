from .readconfig import readconfig

from pathlib import Path
__version__ = open(str(Path(__file__).parent.joinpath('VERSION')))\
              .readline().strip()
