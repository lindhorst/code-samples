from docopt import docopt
import sys
import configparser
import io
import os

# ========================================================================
# Command-line doc string
DOC = """
Usage:
  mycfg [-c <config-file>]
  
Options:
  -c <config-file>, --config <config-file>  Specify a configuration file
"""

# ========================================================================
# Default configuration
CFG_DEFAULT = """
; This is the first section
[SECTION-1]
NAME-1.1 = Val 1.1 DEFAULT
name-1.2 = val 1.2 default

; Here is section II
[section-2]
NAME-2.1 = Val 2.1 DEFAULT
name-2.2 = val 2.2 default
"""

# ========================================================================
# configuration files that will be read if they exist
CFG_FILES = ["/etc/mycfg/mycfg.ini", "~/.mycfg.ini"]


# check that we are running as "main"
print(__name__)
if __name__ != "__main__":
    sys.exit("Please run mycfg as main only")


# read command line options
args = docopt(DOC)
print(args)


# get the default configuration
cfg = configparser.ConfigParser()
cfg.read_string(CFG_DEFAULT)


# read configuration from any existing files in CFG_FILES
for fil in CFG_FILES:
    cfg_fn = os.path.expanduser(fil)
    if os.path.isfile(cfg_fn):
        try:
            cfg_fh = open(cfg_fn, 'r')
        except FileNotFoundError:
            sys.exit(f"File '{cfg_fn}' exists, but could not be opened for reading")
        cfg.read_file(cfg_fh)
        cfg_fh.close()


# read the configuration file specified on the command line
cfg_fn = args['--config']
if cfg_fn is not None:
    try:
        cfg_fh = open(cfg_fn, 'r')
    except FileNotFoundError:
        sys.exit(f"File '{cfg_fn}' could not be opened")
    cfg.read_file(cfg_fh)
    cfg_fh.close()


# print the contents of the config variable
# use a string to simulate a file; write the config to this string and then read
# it back and print it
cfg_f = io.StringIO()
cfg.write(cfg_f)
cfg_f.seek(0)
print(cfg_f.read())
