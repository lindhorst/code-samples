from setuptools import setup

# read version
__version__ = open('readconfig/VERSION').readline().strip()

setup(
    name = 'readconfig',
    description = 'demo of reading ini-style configuration files',
    author = 'Paul Hodor',
    author_email = 'paul@aurynia.com',
    url = 'https://gitlab.com/lindhorst/code-samples',
    license = 'see LICENSE file',
    version = __version__,
    packages = ['readconfig'],
    py_modules = ['mycfg'],
    package_data = {'readconfig': ['LICENSE', 'VERSION']},
    install_requires = [
        'docopt == 0.6.2'
    ],
    entry_points = {
        'console_scripts': {
            'mycfg = mycfg:get_cfg'
        }
    }
)
