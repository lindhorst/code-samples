import sys
import os
from docopt import docopt
from pathlib import Path
import inspect
from pprint import pprint

# get the version
import readconfig
__version__ = readconfig.__version__

from readconfig import readconfig

# ========================================================================
# global constants

# Default configuration
CFG_DEFAULT = """
; This is the first section
[SECTION-1]
NAME-1.1 = Val 1.1 DEFAULT
name-1.2 = val 1.2 default
name-1.3 = val 1.3 default
name-1.4 = val 1.4 default
name-1.5 = val 1.5 default

; Here is section II
[section-2]
NAME-2.1 = Val 2.1 DEFAULT
name-2.2 = val 2.2 default
"""

# configuration files that will be read if they exist
CFG_FILES = ["/etc/mycfg/mycfg.ini", "~/.mycfg.ini"]


# ========================================================================
# main entry point

def get_cfg():

    # name of the executable
    prog = os.path.basename(sys.argv[0])
    
    # docopt string
    doc = f"""
    
    Demonstrates parsing of ini-style configuration files.

    Data are read from the following sources, in increasing order of priority:
      * a string hard-coded in the Python script
      * a system-wide configuration file "{CFG_FILES [0]}"
      * a user configuration file "{CFG_FILES [1]}"
      * a file specified on the command line

    Usage:
      {prog} [-c <config-file>]
      {prog} -v | --version
    
    Options:
      -c <config-file>, --config <config-file>  Specify a configuration file
      -h --help     Show this screen.
      -v --version  Show version.
    """
    doc = inspect.cleandoc(doc)

    # parse the command line
    opts = docopt(doc)

    # if second form was chosen, print the version and exit
    if (opts["--version"]):
        print ("Version: " + __version__)
        sys.exit()

    # make list of files to read
    cfg_files = []
    for fil in CFG_FILES:
        cfg_crt = os.path.expanduser(fil)
        if os.path.isfile(cfg_crt):
            cfg_files.append(cfg_crt)

    if opts["--config"] is not None:
        cfg_files.append(opts["--config"])
            
    # read the configurations
    exit_status, cfg = readconfig(CFG_DEFAULT, cfg_files)

    # print results
    if exit_status != 0:
        print(f"ERROR: readconfig exited with status {exit_status}")
    pprint(cfg)

