# Read configuration files in INI format with the 'configparser' package

The example contains package 'readconfig', which has a function of the same name
that is useful as written. It reads configuration key-value pairs organizxed in
sections (ini format) from a string and a list of files ordered by increasing
priority.

The example also contains a demo executable, which reads configurations from
multiple sources and prints them in json format. The following sources will be
read, in decreasing order of priority:

* a configuration file whose name is specified on the command line
* a user configuration file ```~/.mycfg.ini```
* a system-wide configuration file ```/etc/mycfg/mycfg.ini```
* a default configuration string hard-coded in the script in variable ```CFG_DEFAULT```


## Installation

Follow the method from 09.deployment/02.cli-with-options. This will install an
executable called ```mycfg```. View usage with:

``` bash
mycfg -h
```

## Test

There are a few sample ini files provided for testing. You can try combinations
of system, home, and command-line configuration files.

Install the system configuration file (optionally):

``` bash
cp cfg-system.ini /etc/mycfg/mycfg.ini
```

Install the user configuration file (optionally):

``` bash
cp cfg-home.ini ~/..mycfg.ini
```

Run the program, which will read the hard-coded default configuration and, if
they exist, system and user configuration files:

``` bash
mycfg
```

Run the program with an additional configuration file specified on the command line:

``` bash
mycfg -c cfg-cmd.ini
```
