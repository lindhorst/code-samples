"""Demostrates docstrings in the "reStructured Text" (reST) or Sphinx
    format. This style is used in the official Python documentation.

"""

def say_hello(who = "world"):
    """Print and return a "hello" string

    This docstring is written in the "reStructured Text" (reST) or Sphinx
    format. This style is used in the official Python documentation.

    The function prints and also returns a string containing "hello <who>".

    :param who: the entity that we want to greet
    :type who: string
    :return: hello greeting
    :rtype: string
    """
    msg = "hello " + who
    print("hello " + who)
    return(msg)
