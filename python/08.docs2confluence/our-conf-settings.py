

# our configuration settings
extensions = ['sphinxcontrib.confluencebuilder', 'sphinx.ext.autodoc']
confluence_publish = True
confluence_space_name = 'SDO'
confluence_server_url = 'https://confluence.childrens.sea.kids/'
confluence_ask_user = True
confluence_ask_password = True
confluence_disable_ssl_validation = True
html_theme = 'sphinx_rtd_theme'

confluence_parent_page = 'replace with an actual page name'
