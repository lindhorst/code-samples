# Push Python documentation to Confluence

In this example, the documentation existing in a Python package as docstrings is extracted and published to Confluence automatically. Method:

* create a virtual environment
* install the Sphinx tools (https://www.sphinx-doc.org)
* create the documentation and push it to Confluence

## Create the virtual environment

``` bash
python3 -m venv ./workenv
source ./workenv/bin/activate
pip --trusted-host pypi.python.org install --upgrade pip
```

## Install Atlassian Confluence Builder for Sphinx and dependencies

``` bash
pip --trusted-host pypi.python.org install sphinxcontrib-confluencebuilder
pip --trusted-host pypi.python.org install sphinx_rtd_theme
```

Check installation:

``` bash
python -m sphinxcontrib.confluencebuilder --version
# sphinxcontrib.confluencebuilder 1.2.0
```

## Create the documentation

Make a directory for the new documentation

``` bash
mkdir tmp_hello_demo_doc
```

Create a skeleton configuration.

``` bash
cd tmp_hello_demo_doc
sphinx-quickstart
```

When prompted, make the following choices:

``` ascii
Separate source and build directories (y/n) [n]:
Project name: Hello Demo
Author name(s): <your name>
Project release []:
Project language [en]:
```

Next, we append some custom configuration settings from file ```our-conf-settings.py``` to the newly created configuration file ```conf.py```.

``` bash
cat ../our-conf-settings.py >> ./conf.py
```

Open ```conf.py``` in a text editor and set the value of
"confluence_parent_page" to the name of an actual Confluence page. Make other
tweaks if desired.

Replace file ```index.rst``` with the provided ```our-index.rst```

``` bash
cp ../our-index.rst ./index.rst
```

Set the environment variable PYTHONPATH to the directory containing the package
that we want to document
  
``` bash
export PYTHONPATH="$PWD/../hello_demo"
```

Create the documentation; enter your Children's user name and password when
prompted

``` bash
make confluence
```

Go to Confluence and look at your new page
