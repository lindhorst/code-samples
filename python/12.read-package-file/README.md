# Read files tfrom an isntalled package

In this demo we install package read_file, which contains a couple of data files
in addition to the python module. When running the command from the terminal,
the files are read from their path in the installed package, and their content
is printed out.

## How it works

The data files are in the source directory at `src/read_file/data/subdir/file-{1,2}`. To have them installed with the package, edit the "package-data" line:

```python
package_data = {'read_file': ['LICENSE', 'VERSION', 'data/subdir/*']}
```

Read them with one of 2 methods:

1. package pathlib: relies on file paths relative to that of the executing script
2. package pkg_resources: works by specifying the name of the package and the
   relative path inside the package

## Install

```bash
python3 -m venv workenv
source workenv/bin/activate
pip install --upgrade pip setuptools
pip install src/
```

Installation makes the command `read_file` available.

## Run the command

```bash
read_file
```

Expected output

```text
Command line options:
{'--help': False,
 '--version': False}

content of file 1:
line 2
line 3

content of file 2:
Line A
Line B
```
