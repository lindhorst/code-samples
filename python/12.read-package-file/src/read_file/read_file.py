import sys
import os
from docopt import docopt
from pathlib import Path
import inspect

import pathlib
import pkg_resources

__version__ = open(str(Path(__file__).parent.joinpath('VERSION')))\
              .readline().strip()

# This is the main entry point for the CLI
def read_file():

    # name of the executable
    prog = os.path.basename(sys.argv[0])

    # Write this docopts string following POSIX syntax to define the command
    # line options you want
    doc = f"""
     
    Program "read_file"

    The description of "read_file" goes here

    Usage:
      {prog}
      {prog} -v | --version
      {prog} -h | --help

    Options:
      <arg1>        A required argument
      -v --version  Show version.
      -h --help     Show this screen.
    """
    doc = inspect.cleandoc(doc)

    # Parse the command line based on the docopts string specification, load
    # them into dictionary "opts"
    opts = docopt(doc)
    print("Command line options:")
    print(opts)
    print("")

    # If version was chosen, print it and exit
    if (opts["--version"]):
        print ("Version: " + __version__)
        sys.exit()

    #============================================================
    # demo of reading package files
    #============================================================

    # Method 1
    #   Read from a path relative to that of the current code file
    #   Use package pathlib

    data_1 = pathlib.Path(__file__).parent.joinpath(
        'data/subdir/file-1').read_text()
    print(data_1)
    
    # Method 2
    #   Read from a package
    #   Use package pkg_resources

    fname = pkg_resources.resource_filename(
        'read_file', 'data/subdir/file-2')
    with open(fname, 'r') as f:
        data_2 = f.read()
    print(data_2)
    
