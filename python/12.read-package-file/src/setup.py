from setuptools import setup

__version__ = open('read_file/VERSION').readline().strip()

setup(
    name = 'read_file',
    description = 'Demo how to read data files deployed with a package',
    author = 'Paul Hodor',
    author_email = '',
    url = '',
    license = 'see LICENSE file',
    version = __version__,
    packages = ['read_file'],
    package_data = {'read_file': ['LICENSE', 'VERSION', 'data/subdir/*']},
    install_requires = [
        'docopt == 0.6.2'
    ],
    entry_points = {
        'console_scripts': {
            'read_file = read_file.read_file:read_file'
        }
    }
)
