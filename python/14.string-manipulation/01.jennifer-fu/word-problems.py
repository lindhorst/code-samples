# see https://betterprogramming.pub/the-technical-interview-guide-to-string-manipulation-92f4c4649cd


import string
import re


################################################################################
# isogram

def isIsogram(s):
    s = s.lower()
    return len(s) == len(set(s))


################################################################################
# pangram

def isPangram(s):
    s = s.lower()
    s = {ch for ch in s if ch in string.ascii_lowercase}
    return len(s) == len(string.ascii_lowercase)


################################################################################
# isomorphic

def areIsomorphic(s1, s2):
    if len(s1) != len(s2):
        return False

    tr = {}
    for i in range(len(s1)):
        if s1[i] in tr:
            if tr[s1[i]] != s2[i]:
                return False
        else:
            tr[s1[i]] = s2[i]

    return True


################################################################################
# anagrams

def showAnagrams(source, pool):
    def check_ana(s1, s2):
        l1 = list(s1.lower())
        l1.sort()
        l2 = list(s2.lower())
        l2.sort()
        return l1 == l2
    
    l = len(pool)
    return {source[i:(i+l)] for i in range(0, (len(source) - l + 1))
            if check_ana(source[i:(i+l)], pool)}


################################################################################
# palindrome

def maxPalindrome1(s):
    s = re.sub('\W', '', s)
    l = len(s)
    
    if l < 2:
        return s

    pal = s[0]
    for i in range(l):
        # even
        j = 0
        while i - j - 1 >= 0 and \
              i + j < l and \
              s[i - j - 1].lower() == s[i + j].lower():
            j += 1
        crt = s[(i - j):(i + j)]
        if len(crt) > len(pal):
            pal = crt

        # odd
        j = 1
        while i - j >= 0 and \
              i + j < l and \
              s[i - j].lower() == s[i + j].lower():
            j += 1
        crt = s[(i - j + 1):(i + j)]
        if len(crt) > len(pal):
            pal = crt

    return pal


################################################################################
# driver

if __name__ == '__main__':
    print('\n== isograms:')
    s = ''
    print(f'"{s}": {isIsogram(s)}')
    s = 'a'
    print(f'"{s}": {isIsogram(s)}')
    s = 'complexity'
    print(f'"{s}": {isIsogram(s)}')
    s = 'complexities'
    print(f'"{s}": {isIsogram(s)}')

    print('\n== pangram:')
    s = ''
    print(f'"{s}": {isPangram(s)}') # false
    s = 'Bawds jog, flick quartz, vex nymphs.'
    print(f'"{s}": {isPangram(s)}') # true
    s = 'The quick brown fox jumped over the lazy sleeping dog.'
    print(f'"{s}": {isPangram(s)}') # true
    s = 'Roses are red, violets are blue, sugar is sweet, and so are you.'
    print(f'"{s}": {isPangram(s)}') # false
    
    print('\n== isomorphic:')
    s1 = 'abc'
    s2 = 'ab'
    print(f'"{s1}", "{s2}": {areIsomorphic(s1, s2)}')
    s1 = 'atlatl'
    s2 = 'tartar'
    print(f'"{s1}", "{s2}": {areIsomorphic(s1, s2)}')
    s1 = 'atlatlp'
    s2 = 'tartarq'
    print(f'"{s1}", "{s2}": {areIsomorphic(s1, s2)}')
    s1 = 'atlatlpb'
    s2 = 'tartarqc'
    print(f'"{s1}", "{s2}": {areIsomorphic(s1, s2)}')
    s1 = 'atlatlpa'
    s2 = 'tartarqb'
    print(f'"{s1}", "{s2}": {areIsomorphic(s1, s2)}')

    print('\n== anagrams:')
    s = 'AaaAAaaAAaa'
    p = 'aa'
    print(f'"{s}", "{p}": {showAnagrams(s, p)}')
    s = 'CbatobaTbacBoat'
    p = 'Boat'
    print(f'"{s}", "{p}": {showAnagrams(s, p)}')
    s = 'AyaKkayakkAabkk'
    p = 'Kayak'
    print(f'"{s}", "{p}": {showAnagrams(s, p)}')

    print('\n== palindrome:')
    maxPalindrome = maxPalindrome1
    s = ''
    print(f'"{s}": {maxPalindrome(s)}')
    s = 'abc'
    print(f'"{s}": {maxPalindrome(s)}')
    s = 'Aabcd'
    print(f'"{s}": {maxPalindrome(s)}')
    s = 'I am Bob.'
    print(f'"{s}": {maxPalindrome(s)}')
    s = 'Odd or even'
    print(f'"{s}": {maxPalindrome(s)}')
    s = 'Never odd or even'
    print(f'"{s}": {maxPalindrome(s)}')
    s = 'Today is 02/02/2020.'
    print(f'"{s}": {maxPalindrome(s)}')
    s = 'It is 2/20/2020.'
    print(f'"{s}": {maxPalindrome(s)}')
    s = 'A man, a plan, a canal – Panama'
    print(f'"{s}": {maxPalindrome(s)}')
