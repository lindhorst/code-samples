################################################################################
# Longest Substring Without Repeating Characters
#
# Given a string s, find the length of the longest substring without
# repeating characters.


# this is too slow
class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        res = 0
        i = 0
        while i < len(s):
            lett = {}
            for j in range(i, len(s)):
                if s[j] in lett:
                    if len(lett) > res:
                        res = len(lett)
                    i = lett[s[j]]
                    break
                else:
                    lett[s[j]] = j
            if len(lett) > res:
                res = len(lett)
            i += 1
            if len(s) - i < res:
                break

        return res


#===============================================================================
# driver

if __name__ == '__main__':
    s = Solution()
    print(s.lengthOfLongestSubstring("abcabcbb")) # 3
    print(s.lengthOfLongestSubstring("bbbbb"))    # 1
    print(s.lengthOfLongestSubstring("pwwkew"))   # 3
    print(s.lengthOfLongestSubstring(" "))        # 1
    print(s.lengthOfLongestSubstring("aab"))       # 2
    
