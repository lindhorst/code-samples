################################################################################
# 3sum

# Given an integer array nums, return all the triplets [nums[i],
# nums[j], nums[k]] such that i != j, i != k, and j != k, and nums[i]
# + nums[j] + nums[k] == 0.

# Notice that the solution set must not contain duplicate triplets.


class Solution_1:
    def threeSum(self, nums: list[int]) -> list[list[int]]:
        nums = sorted(nums)
        res = set()
        for i in range(len(nums) - 2):
            if nums[i] > 0:
                break
            for j in range(i + 1, len(nums) - 1):
                if nums[i] + nums[j] > 0:
                    break
                for k in range(j + 1, len(nums)):
                    mysum = nums[i] + nums[j] + nums[k]
                    if mysum > 0:
                        break
                    if mysum == 0:
                        res.add((nums[i],nums[j],nums[k]))
        return [[i,j,k] for i,j,k in res]
        

class Solution:
    def threeSum(self, nums: list[int]) -> list[list[int]]:
        if len(nums) < 3:
            return []

        htmp = {}
        for x in nums:
            if x not in htmp:
                htmp[x] = 1
            else:
                htmp[x] += 1
        mynums = []
        for k in htmp:
            maxrep = 2
            if k == 0:
                maxrep = 3
            rep = min(htmp[k], maxrep)
            mynums.extend([k] * rep)
        mynums.sort()

        res = set()

        for i in range(len(mynums) - 2):
            x = mynums[i]
            if x > 0:
                break
            h = set()
            for j in range(i + 1, len(mynums)):
                y = mynums[j]
                z = (-x) + (-y)
                if z in h:
                    res.add((x,y,z))
                else:
                    h.add(y)

        return [[x,y,z] for x,y,z in res]

    
# somebody else did this:    
class Solution_2:
    def threeSum(self, nums: list[int]) -> list[list[int]]:
        nums.sort()
        ans = []

        for i, a in enumerate(nums):
            if a > 0:
                break
            
            if i > 0 and a == nums[i - 1]:
                continue
            
            l = i + 1
            r = len(nums) - 1

            while l < r:
                threeSum = a + nums[l] + nums[r]

                if threeSum > 0: # Too big
                    r -= 1
                elif threeSum < 0: # Too small
                    l += 1
                else:
                    ans.append([a, nums[l], nums[r]])
                    l += 1
                    r -= 1
                    

                    while nums[l] == nums[l - 1] and l < r:
                        l += 1
            
        return ans


#===============================================================================
# driver

if __name__ == '__main__':
    s = Solution()
    print(s.threeSum([-1,0,1,2,-1,-4])) # [[-1,-1,2],[-1,0,1]]
    print(s.threeSum([0,1,1]))          # []
    print(s.threeSum([0,0,0]))          # [[0,0,0]]
    print(s.threeSum([0,0,0,0,0,0]))    # [[0,0,0]]
    
    
    
