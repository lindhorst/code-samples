################################################################################
# Longest Palindromic Substring
#
# Given a string s, return the longest palindromic substring in s.


class Solution:
    def longestPalindrome(self, s: str) -> str:
        l = len(s)

        if l < 2:
            return s

        pal = s[0]
        for i in range(1, l):
            # even
            j = 0
            while i - j - 1 >= 0 and \
                  i + j < l and \
                  s[i - j - 1].lower() == s[i + j].lower():
                j += 1
            crt = s[(i - j):(i + j)]
            if len(crt) > len(pal):
                pal = crt

            # odd
            j = 1
            while i - j >= 0 and \
                  i + j < l and \
                  s[i - j].lower() == s[i + j].lower():
                j += 1
            crt = s[(i - j + 1):(i + j)]
            if len(crt) > len(pal):
                pal = crt

        return pal



#===============================================================================
# driver

if __name__ == '__main__':
    s = Solution()
    print(s.longestPalindrome('babad'))       # bab or aba
    print(s.longestPalindrome('bb'))          # bb
    print(s.longestPalindrome('abracadabra')) # aca or ada
