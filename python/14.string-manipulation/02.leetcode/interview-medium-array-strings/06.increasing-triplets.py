################################################################################
# Increasing Triplet Subsequence
#
# Given an integer array nums, return true if there exists a triple of
# indices (i, j, k) such that i < j < k and nums[i] < nums[j] <
# nums[k]. If no such indices exists, return false.


class Solution:
    def increasingTriplet(self, nums: list[int]) -> bool:
        # sol = []
        # 
        # for i in range(len(nums)):
        #     found = False
        #     for j in range(len(sol)):
        #         val, cnt = sol[j]
        #         if nums[i] > val:
        #             found = True
        #             if cnt == 2:
        #                 return True
        #             else:
        #                 sol[j] = (nums[i], cnt + 1)
        #             break
        #     if not found:
        #         sol.append((nums[i], 1))

        return False


#===============================================================================
# driver

if __name__ == '__main__':
    s = Solution()
    print(s.increasingTriplet([1,2,3,4,5]))   # True
    print(s.increasingTriplet([5,4,3,2,1]))   # False
    print(s.increasingTriplet([2,1,5,0,4,6])) # True
    
