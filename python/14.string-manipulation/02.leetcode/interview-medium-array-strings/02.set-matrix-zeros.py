################################################################################
# Set Matrix Zeros
#
# Given an m x n integer matrix matrix, if an element is 0, set its
# entire row and column to 0's.
#
# You must do it in place.
#
# m == matrix.length, number of rows
# n == matrix[0].length, number of columns

class Solution:
    def zeroRow(self, mat: list[list[int]], r: int) -> None:
        m = len(mat)
        if len(mat) == 0:
            return
        n = len(mat[0])

        if r >= 0 and r < m:
            mat[r] = [0] * n
        
    def zeroCol(self, mat: list[list[int]], c: int) -> None:
        m = len(mat)
        if len(mat) == 0:
            return
        n = len(mat[0])

        if c >= 0 and c < n:
            for i in range(m):
                mat[i][c] = 0

    def setZeroes(self, matrix: list[list[int]]) -> None:
        """
        Do not return anything, modify matrix in-place instead.
        """

        if len(matrix) == 0:
            return
        
        # find zeros
        zr = set()
        zc = set()
        for i in range(len(matrix)):
            for j in range(len(matrix[0])):
                if matrix[i][j] == 0:
                    zr.add(i)
                    zc.add(j)

        # set zeros
        for r in zr:
            self.zeroRow(matrix, r)
        for c in zc:
            self.zeroCol(matrix, c)


#===============================================================================
# driver

if __name__ == '__main__':
    s = Solution()
    matrix = [[1,1,1],[1,0,1],[1,1,1]]
    s.setZeroes(matrix)
    print(matrix)
    # [[1,0,1],[0,0,0],[1,0,1]]

    matrix = [[0,1,2,0],[3,4,5,2],[1,3,1,5]]
    s.setZeroes(matrix)
    print(matrix)
    # [[0,0,0,0],[0,4,5,0],[0,3,1,0]]
