################################################################################
# Group Anagrams
#
# Given an array of strings strs, group the anagrams together. You can
# return the answer in any order.
#
# An Anagram is a word or phrase formed by rearranging the letters of
# a different word or phrase, typically using all the original letters
# exactly once.


# the defaultdict on a list supports the 'append' operation when the
# key is not present
from collections import defaultdict
class Solution:
    def groupAnagrams(self, strs: list[str]) -> list[list[str]]:
        m = defaultdict(list)
        for s in strs:
            m[''.join(sorted(list(s)))].append(s)
        return [m[k] for k in m]


#===============================================================================
# driver

if __name__ == '__main__':
    s = Solution()
    print(s.groupAnagrams(["eat","tea","tan","ate","nat","bat"]))
    # [["bat"],["nat","tan"],["ate","eat","tea"]]
    print(s.groupAnagrams([""]))
    # [[""]]
    print(s.groupAnagrams(["a"]))
    # [["a"]]
