################################################################################
# palindrome
#
# A phrase is a palindrome if, after converting all uppercase letters
# into lowercase letters and removing all non-alphanumeric characters,
# it reads the same forward and backward. Alphanumeric characters
# include letters and numbers.
#
# Given a string s, return true if it is a palindrome, or false
# otherwise.

import re

class Solution:
    def isPalindrome(self, s: str) -> bool:
        s = re.sub('[^a-zA-Z0-9]', '', s).lower()

        # empty string
        if len(s) == 0:
            return True

        for i in range(len(s)//2):
            if s[i] != s[-(i+1)]:
                return False

        return True


#===============================================================================
# driver

if __name__ == '__main__':
    s = Solution()
    print(s.isPalindrome("A man, a plan, a canal: Panama")) # True
    print(s.isPalindrome("race a car"))                     # False
    print(s.isPalindrome("; ."))                            # True
    print(s.isPalindrome("ab_a"))                           # True
    
