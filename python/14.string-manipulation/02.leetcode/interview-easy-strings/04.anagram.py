################################################################################
# Anagram
#
# Given two strings s and t, return true if t is an anagram of s, and
# false otherwise.
#
# An Anagram is a word or phrase formed by rearranging the letters of
# a different word or phrase, typically using all the original letters
# exactly once.

class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        if len(s) != len(t):
            return False
        
        ls = list(s)
        ls.sort()
        lt = list(t)
        lt.sort()

        return ls == lt



#===============================================================================
# driver

if __name__ == '__main__':
    s = Solution()
    print(s.isAnagram("anagram", "nagaram")) # True
    print(s.isAnagram("rat", "car"))         # False
    
