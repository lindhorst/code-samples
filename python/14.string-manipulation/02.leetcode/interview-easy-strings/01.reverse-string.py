################################################################################
# Reverse String
#
# https://leetcode.com/explore/interview/card/top-interview-questions-easy/127/strings/879/
#
# Write a function that reverses a string. The input string is given
# as an array of characters s.
#
# You must do this by modifying the input array in-place with O(1)
# extra memory.

class Solution:
    def reverseString(self, s: list[str]) -> None:
        for i in (range(len(s)//2)):
            j = len(s) - i - 1
            tmp = s[i]
            s[i] = s[j]
            s[j] = tmp


#===============================================================================
# driver

if __name__ == '__main__':
    s = Solution()
    l = ["h","e","l","l","o"]
    s.reverseString(l)
    print(l)
    l = ["H","a","n","b","a","h"]
    s.reverseString(l)
    print(l)
