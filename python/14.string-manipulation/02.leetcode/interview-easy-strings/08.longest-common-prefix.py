################################################################################
# Longest Common Prefix
#
# Write a function to find the longest common prefix string amongst an
# array of strings.
#
# If there is no common prefix, return an empty string "".

class Solution_1:
    def longestCommonPrefix(self, strs: list[str]) -> str:
        if len(strs) == 0:
            return ''
        if len(strs) == 1:
            return strs[0]
        
        maxLen = min([len(s) for s in strs])
        if maxLen == 0:
            return ''

        reslen = 0
        iMin = 0
        iMax = maxLen
        while iMin < iMax:
            i = (iMin + iMax)//2 + 1
            isPrefix = all([s[:i] == strs[0][:i] for s in strs])
            if isPrefix:
                reslen = i
                iMin = i
            else:
                iMax = i - 1

        
        return strs[0][:reslen]

# somebody wrote this very smart solution
class Solution:
    def longestCommonPrefix(self, v: list[str]) -> str:
        ans=""
        v=sorted(v)
        first=v[0]
        last=v[-1]
        for i in range(min(len(first),len(last))):
            if(first[i]!=last[i]):
                return ans
            ans+=first[i]
        return ans

#===============================================================================
# driver

if __name__ == '__main__':
    s = Solution()
    print(s.longestCommonPrefix(["flower","flow","flight"])) # fl
    print(s.longestCommonPrefix(["dog","racecar","car"])) # ''
