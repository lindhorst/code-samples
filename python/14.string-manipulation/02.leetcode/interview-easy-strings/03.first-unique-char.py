################################################################################
# First Unique Character in a String
#
# Given a string s, find the first non-repeating character in it and
# return its index. If it does not exist, return -1.

class Solution:
    def firstUniqChar(self, s: str) -> int:
        letters = set(s)
        singl = {l for l in letters if s.count(l) ==1}
        for i in range(len(s)):
            if s[i] in singl:
                return i
        
        return -1

#===============================================================================
# driver

if __name__ == '__main__':
    s = Solution()
    print(s.firstUniqChar("leetcode"))     # 0
    print(s.firstUniqChar("loveleetcode")) # 2
    print(s.firstUniqChar("aabb"))         # -1
