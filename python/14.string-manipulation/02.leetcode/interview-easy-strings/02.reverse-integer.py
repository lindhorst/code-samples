################################################################################
# Reverse Integer
#
# Given a signed 32-bit integer x, return x with its digits
# reversed. If reversing x causes the value to go outside the signed
# 32-bit integer range [-2^31, 2^31 - 1], then return 0.
#
# Assume the environment does not allow you to store 64-bit integers
# (signed or unsigned).

class Solution:
    def reverse(self, x: int) -> int:
        intMin = -2147483648
        intMax = 2147483647
        
        # make reverse as string
        l = list(str(x))
        sign = 1
        if x < 0:
            l = l[1:]
            sign = -1
        l.reverse()
        s = ''.join(l)

        # check overflow
        if len(s) == 10:
            sMax = str(intMax)
            over = False
            under = False
            i = 0
            while not over and not under and i < len(s):
                if s[i] > sMax[i]:
                    over = True
                elif s[i] < sMax[i]:
                    under = True
                i += 1
            if over:
                return 0
        
        return sign*int(s)
        

#===============================================================================
# driver

if __name__ == '__main__':
    s = Solution()
    print(s.reverse(123))         # 321  
    print(s.reverse(-123))        # -321 
    print(s.reverse(120))         # 21   
    print(s.reverse(-120))        # -21  
    print(s.reverse(-2147483648)) # 0    
    print(s.reverse(-2147483640)) # -463847412    
    print(s.reverse(2147483647))  # 0    
    print(s.reverse(2147483640))  # 463847412
    print(s.reverse(2147483512))  # 0
