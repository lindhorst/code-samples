################################################################################
# String to integer (atoi)
#
# The algorithm for myAtoi(string s) is as follows:
#
#    1.  Whitespace: Ignore any leading whitespace (" ").
#        
#    2.  Signedness: Determine the sign by checking if the next character
#        is '-' or '+', assuming positivity is neither present.
#        
#    3.  Conversion: Read the integer by skipping leading zeros until a
#        non-digit character is encountered or the end of the string is
#        reached. If no digits were read, then the result is 0.
#        
#    4.  Rounding: If the integer is out of the 32-bit signed integer
#        range [-231, 231 - 1], then round the integer to remain in the
#        range. Specifically, integers less than -231 should be rounded
#        to -231, and integers greater than 231 - 1 should be rounded to
#        231 - 1.
#
# Return the integer as the final result.

import re

class Solution:
    def myAtoi(self, s: str) -> int:
        intMin = -2147483648
        intMax = 2147483647
        
        # extract the number
        myre = '\\s*(\\+?|-)0*([0-9]+)'
        m = re.match(myre, s)

        # prepare result
        res = 0
        if m is not None:
            (sign, num) = m.groups()
            num = num[:11]
            res = int(sign + num)
            if res < intMin:
                res = intMin
            elif res > intMax:
                res = intMax
        
        return res


#===============================================================================
# driver

if __name__ == '__main__':
    s = Solution()
    print(s.myAtoi('   42'))         # 42
    print(s.myAtoi('-042'))          # -42
    print(s.myAtoi('1337c0d3'))      # 1337
    print(s.myAtoi('2-1'))           # 2
    print(s.myAtoi('words and 987')) # 0
    print(s.myAtoi('+3.14'))         # 3
    print(s.myAtoi('0'))             # 0
    print(s.myAtoi('+0'))            # 0
    print(s.myAtoi('-0'))            # 0
    print(s.myAtoi('1111111111111')) # 2147483647
    print(s.myAtoi('-111111111111')) # -2147483648

    
