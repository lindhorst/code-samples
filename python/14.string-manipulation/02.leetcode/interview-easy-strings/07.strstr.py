################################################################################
# Implement strStr()
#
# Given two strings needle and haystack, return the index of the first
# occurrence of needle in haystack, or -1 if needle is not part of
# haystack.


# class Solution:
#     def strStr(self, haystack: str, needle: str) -> int:
#         return(haystack.find(needle))

class Solution:
    def strStr(self, haystack: str, needle: str) -> int:
        lh = len(haystack)
        ln = len(needle)
        for i in range(lh - ln + 1):
            match = True
            for j in range(ln):
                if haystack[i+j] != needle[j]:
                    match = False
                    break
            if match:
                return i
        return -1

    
#===============================================================================
# driver

if __name__ == '__main__':
    s = Solution()
    print(s.strStr('abracadabra', 'bra')) # 1
    print(s.strStr('abracadabra', 'brb')) # -1
