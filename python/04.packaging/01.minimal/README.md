# Packaging a minimal application

This directory contains a single package, "my_hello", with a single module,
"hello.py". The setup.py script sets the package name and version only. Normally
the name in setup.py should match the name of the package directory. Here they
are different, just so the different names can be tracked during usage.

## Import the source directory

In this scenario changes to the code are immediately available in the virtual
environment in which the package is installed.

### <a id="create_venv"></a>Create a virtual environment

``` bash
python3 -m venv ./workenv
source ./workenv/bin/activate
pip install --upgrade pip setuptools
```

### Install the package from local directory

``` bash
pip install .
```

Check name and version of the installed package
``` bash
pip freeze | grep mini
# myminimal==0.0.99
```

### Test package functionality

Call the module directly:
``` bash
python -m my_hello.hello
# -- Running as main --
# hello world
```

Now edit source file "hello.py", changing line 3 from

```python
the_world = "world"
```

to

```python
the_world = "worldz"
```

The output of the previous command is now different:
``` bash
python -m my_hello.hello
# -- Running as main --
# hello worldz
```

To test the functionality of the package in an interactive session, start a
session by typing in "python". Then try things like the following:

```
>>> import my_hello.hello
>>> my_hello.hello.say_hello()
hello worldz
>>> from my_hello.hello import say_hello as hey
>>> hey()
hello worldz
```

### Clean up

Change the "hello.py" source file back to say "world".

Delete the virtual environment

``` bash
deactivate
rm -r ./workenv
```

## Use a package archive

### Create a virtual environment

See [above](#create_venv).

### Create package archive

Build both a source and binary distribution:

```bash
python setup.py sdist bdist
```

Two archive files are created in directory ```dist```:

* ```myminimal-0.0.99.linux-x86_64.tar.gz```
* ```myminimal-0.0.99.tar.gz```

The source distribution seems to be more useful:

```bash
tar tzf dist/myminimal-0.0.99.tar.gz
# myminimal-0.0.99/
# myminimal-0.0.99/setup.py
# myminimal-0.0.99/setup.cfg
# myminimal-0.0.99/myminimal.egg-info/
# myminimal-0.0.99/myminimal.egg-info/dependency_links.txt
# myminimal-0.0.99/myminimal.egg-info/SOURCES.txt
# myminimal-0.0.99/myminimal.egg-info/PKG-INFO
# myminimal-0.0.99/myminimal.egg-info/top_level.txt
# myminimal-0.0.99/PKG-INFO
# myminimal-0.0.99/my_hello/
# myminimal-0.0.99/my_hello/hello.py
```

### Install the package from the archive

```bash
pip install dist/myminimal-0.0.99.tar.gz
```

The rest of testing, etc. is the same as above for installation from local
directory.
