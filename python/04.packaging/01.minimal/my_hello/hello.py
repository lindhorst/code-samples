# a global variablea
global_var = 25
the_world = "world"

# a function
def say_hello(who=the_world):
    print ("hello " + who)

if __name__ == '__main__':
    print ("-- Running as main --")
    say_hello()
