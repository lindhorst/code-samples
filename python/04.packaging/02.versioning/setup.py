from setuptools import setup

# read version
__version__ = open('my_hello/VERSION').readline().strip()

setup(
    name = "my_hello",
    description = "demo of packaging with metadata",
    author = 'Paul Hodor',
    author_email = 'paul@aurynia.com',
    url='https://gitlab.com/paul.hodor/code-samples',
    license = 'see LICENSE file',
    version = __version__,
    packages = ['my_hello'],
    package_data = {'my_hello': ['LICENSE', 'VERSION']}
)
