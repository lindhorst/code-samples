# Packaging an application that has a version number and other metadata

The version number is stored in file ```VERSION``` in the package directory. It
can't be stored at the top level, because it then gets installed at the top
level in the "site packages" subdirectory", meaning files from different
packages will overwrite one another. This file, along with ```LICENSE```
and ```README.txt``` are added to the distribution package. A few additional
metadata fields are specified in the setup file.

Create a virtual environment similar to that in the 01.minimal example and
install the package.


``` bash
python3 -m venv ./workenv
source ./workenv/bin/activate
pip install --upgrade pip setuptools
```

Create the package

```bash
python setup.py sdist
```

Install the package

```bash
pip install dist/my_hello-0.1.2.tar.gz
```

Check the installed package and its metadata:

```
pip freeze
# my-hello==0.1.2
# pkg-resources==0.0.0

pip show my-hello
# Name: my-hello
# Version: 0.1.2
# Summary: demo of packaging with metadata
# Home-page: https://gitlab.com/paul.hodor/code-samples
# Author: Paul Hodor
# Author-email: paul@aurynia.com
# License: see LICENSE file
# Location: /home/paul/data/17.programming-code/git-samples/python/04.packaging/02.versioning/workenv/lib/python3.5/site-packages
# Requires: 
# Required-by: 
```

Check version in an interactive session

```
>>> import my_hello
>>> my_hello.__version__
'0.1.2'
```
