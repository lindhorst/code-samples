#!/usr/bin/env python

# Hello world program that demonstrates docopt as a means to specify and use
# command line options

__version__ = "0.99.0"

import sys
from docopt import docopt

# Write this docopts string following POSIX syntax to define the command line
# options you want
doc="""

A "Hello world" program

Demonstrates working with a python virtual environment with required packages

Usage:
  hello.py [--goodbye] [<name>]
  hello.py -v | --version

Options:
  --goodbye     Add "goodbye message"
  -h --help     Show this screen.
  -v --version  Show version.
"""

# Parse the command line based on the docopts string specification, load them
# into dictionary "opts"
opts = docopt(doc)
print("Command line options:")
print(opts)
print("")

# If second form was chosen, print the version and exit
if (opts["--version"]):
    print ("Version: " + __version__)
    sys.exit

# If first form was chosen, print the "hello world" message
msg = "hello "
subj = "world"
if (opts["--goodbye"]):
    msg = msg + "and goodbye "
if (opts["<name>"] is not None):
    subj = opts["<name>"]
print(msg + subj)
