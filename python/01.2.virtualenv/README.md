# Example of a Python virtual environment with package requirements

A list of packages that need to be installed in a virtual environment can be
provided in file ```requirements.txt```. This list can be created by hand, or
generated from an environment in which packages have been installed manually.

## Generate a list of requirements

First, install and activate a virtual environment as described in
01.1.virtualenv.

Install packages manually:
``` bash
pip install docopt
pip list
```

Generate requirements file:
``` bash
pip freeze > requirements.txt
```

Finish:

``` bash
deactivate
```

## Setup virtual environment based on the requirements file
``` bash
python3 -m venv ./workenv
source ./workenv/bin/activate
pip install --upgrade pip setuptools
pip install -r requirements.txt
deactivate
```

## Run a script

``` bash
source <path/to/venv/directory>/bin/activate
python <script>.py
<path/to>/<script>.py
deactivate
```

Example
``` bash
source ./workenv/bin/activate
./hello.py -v
./hello.py
./hello.py --goodbye
./hello.py paul
./hello.py --goodbye paul
deactivate
```
