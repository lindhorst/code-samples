# Deploy a Python command line tool in a virtual environment

The example is almost minimal. It installs a command line tool that prints out
the python version and the path to the python executable. The path shows that
python is called in the virtual environment and not from the system location.

We are installing here module ```hello``` from package ```my_hello```. It will
make available a command named ```hello_cli```. Normally the package name should
be the same as the module name, but here they are different so that we can
better track the different objects. In addition, if we want to install a single
command, its name may be the same as that of the package/module as well.

## Installation steps

### Prerequisites

This should run on any POSIX system with an appropriate version of Python 3
installed. It was tested with Python 3.6.4, but other versions of Python 3 may
work as well.

### Create a virtual environment

This can be done at an arbitrary directory path.

``` bash
python3 -m venv ./my_virtual_environment
source ./my_virtual_environment/bin/activate
pip install --upgrade pip setuptools
```

### Install the my_hello package

At this point we are still working with the virtual environment activated.

We need the path to the directory containing the ```my_hello``` package and
file ```setup.py```.

Run:

``` bash
pip install /path/to/where/setup.py/is
```

### Deactivate the virtual environment

``` bash
deactivate
```

### (Optional) Create a link to the executable command from a directory that is on your path

This step will put the command on your path, which will allow executing it
directly from a terminal.

Identify a directory that is listed in your $PATH variable. For
example ```/opt/bin``` or ```~/bin```. In this example we will use ```~/bin```.

Create a symbolic link:

``` bash
ln -s /path/to/my_virtual_environment/bin/hello_cli ~/bin
```

## Use the command directly

The command ```hello_cli``` is ready to go. You do not need to first activate
the virtual environment.

The command can be run by specifying the absolute path to its location in the
virtual environment:

``` bash
/path/to/my_virtual_environment/bin/hello_cli
```

If the command was linked to a directory on your path, the command can be run as

``` bash
hello_cli
```
