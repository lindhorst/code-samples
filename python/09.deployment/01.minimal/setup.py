from setuptools import setup

setup(
    packages = ['my_hello'],
    entry_points = {
        'console_scripts': {
            'hello_cli = my_hello.hello:say_hello'
        }
    }
)
