import sys

# This is the main entry point for the CLI
def say_hello():
    print("\nHello world\n")
    print("Python version:")
    print(sys.version)
    print("")
    print("Executable:")
    print(sys.executable)
