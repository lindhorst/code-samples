from setuptools import setup

# read version
__version__ = open('my_hello/VERSION').readline().strip()

setup(
    name = 'my_hello',
    description = 'demo of deployment with pip in a virtual environment',
    author = 'Paul Hodor',
    author_email = 'paul@aurynia.com',
    url='https://gitlab.com/paul.hodor/code-samples',
    license = 'see LICENSE file',
    version = __version__,
    packages = ['my_hello'],
    package_data = {'my_hello': ['LICENSE', 'VERSION']},
    install_requires = [
        'docopt == 0.6.2'
    ],
    entry_points = {
        'console_scripts': {
            'hello_cli = my_hello.hello:say_hello'
        }
    }
)
