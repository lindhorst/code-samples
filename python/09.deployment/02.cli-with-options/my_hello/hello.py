import sys
import os
from docopt import docopt
from pathlib import Path
import inspect

__version__ = open(str(Path(__file__).parent.joinpath('VERSION')))\
              .readline().strip()

# This is the main entry point for the CLI
def say_hello():
    
    # name of the executable
    prog = os.path.basename(sys.argv[0])
    
    # Write this docopts string following POSIX syntax to define the command
    # line options you want
    doc = f"""
     
    A "Hello world" program
     
    Demonstrates deployment of a CLI written with Python
     
    Usage:
      {prog} [--goodbye] [<name>]
      {prog} -v | --version
    
    Options:
      --goodbye     Add "goodbye message"
      -h --help     Show this screen.
      -v --version  Show version.
    """
    doc = inspect.cleandoc(doc)

    # Parse the command line based on the docopts string specification, load
    # them into dictionary "opts"
    opts = docopt(doc)
    print("Command line options:")
    print(opts)
    print("")

    # If second form was chosen, print the version and exit
    if (opts["--version"]):
        print ("Version: " + __version__)
        sys.exit()

    # If first form was chosen, print the "hello world" message
    msg = "hello "
    subj = "world"
    if (opts["--goodbye"]):
        msg = msg + "and goodbye "
    if (opts["<name>"] is not None):
        subj = opts["<name>"]

    print(msg + subj)

# This section allows calling the module explicitly:
#   python -m my_hello.hello
if __name__ == '__main__':
    print ("\n-- Running as main --")
    say_hello()

