import sys
import os
from docopt import docopt
from pathlib import Path
import inspect

__version__ = open(str(Path(__file__).parent.joinpath('VERSION')))\
              .readline().strip()

def cps():
    
    #===========================================================================
    # command line
    prog = os.path.basename(sys.argv[0])
    
    doc = f"""
     
    Create Package Skeleton
     
    When beginning the development of a new Python package, this command is
    useful to create a standard directory structure and a few skeleton files.
     
    Usage:
      {prog} <new-package-name> [-u]
      {prog} -v | --version
      {prog} -h | --help
    
    Options:
      <new-package-name>  The name of the package - it corresponds to the name
                          of a subdirectory that will be created in the current
                          directory
      -u                  Under the tests directory, output a unittest skeleton,
                          instead of the default pytest
      -v --version        Show version.
      -h --help           Show this screen.
    """
    doc = inspect.cleandoc(doc)

    opts = docopt(doc)

    
    #===========================================================================
    # version
    if (opts["--version"]):
        print ("Version: " + __version__)
        sys.exit()

        
    #===========================================================================
    # create package skeleton
    pck = opts["<new-package-name>"]
    dir_src = os.fspath(f'./{pck}')
    dir_pck = os.fspath(f'./{pck}/{pck}')
    dir_tst = os.fspath(f'./{pck}/tests/{pck}')
    
    os.makedirs(dir_pck)
    os.makedirs(dir_tst)

    #---------------------------------------------------------------------------
    # setup.py
    s = f'''
    from setuptools import setup

    __version__ = open('{pck}/VERSION').readline().strip()
    
    setup(
        name = '{pck}',
        description = '',
        author = '',
        author_email = '',
        url = '',
        license = 'see LICENSE file',
        version = __version__,
        packages = ['{pck}'],
        package_data = {{'{pck}': ['LICENSE', 'VERSION']}},
        install_requires = [
            'docopt == 0.6.2'
        ],
        entry_points = {{
            'console_scripts': {{
                '{pck} = {pck}.{pck}:{pck}'
            }}
        }}
    )
    '''
    s = inspect.cleandoc(s) + '\n'
    f = os.open(os.fspath(f'{dir_src}/setup.py'), os.O_RDWR|os.O_CREAT)
    os.write(f, bytes(s, 'utf-8'))
    os.close(f)

    #---------------------------------------------------------------------------
    # LICENSE
    s = '''
    Copyright <YEAR> <COPYRIGHT HOLDER>
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to
    deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
    '''
    s = inspect.cleandoc(s) + '\n'
    f = os.open(os.fspath(f'{dir_pck}/LICENSE'), os.O_RDWR|os.O_CREAT)
    os.write(f, bytes(s, 'utf-8'))
    os.close(f)

    #---------------------------------------------------------------------------
    # VERSION
    s = '0.0.0'
    f = os.open(os.fspath(f'{dir_pck}/VERSION'), os.O_RDWR|os.O_CREAT)
    os.write(f, bytes(s, 'utf-8'))
    os.close(f)

    #---------------------------------------------------------------------------
    # __init__.py
    s = '''
    from pathlib import Path
    __version__ = open(str(Path(__file__).parent.joinpath('VERSION')))\\
                  .readline().strip()

    '''
    s = inspect.cleandoc(s)
    f = os.open(os.fspath(f'{dir_pck}/__init__.py'), os.O_RDWR|os.O_CREAT)
    os.write(f, bytes(s, 'utf-8'))
    os.close(f)

    #---------------------------------------------------------------------------
    # {pck}.py
    s = f'''
    import sys
    import os
    from docopt import docopt
    from pathlib import Path
    import inspect
    
    __version__ = open(str(Path(__file__).parent.joinpath('VERSION')))\\
                  .readline().strip()
    
    # This is the main entry point for the CLI
    def {pck}():
    
        # name of the executable
        prog = os.path.basename(sys.argv[0])
    
        # Write this docopts string following POSIX syntax to define the command
        # line options you want
        doc = f"""
         
        Program "{{prog}}"
    
        The description of "{{prog}}" goes here
    
        Usage:
          {{prog}} <arg1>
          {{prog}} -v | --version
          {{prog}} -h | --help
    
        Options:
          <arg1>        A required argument
          -v --version  Show version.
          -h --help     Show this screen.
        """
        doc = inspect.cleandoc(doc)
    
        # Parse the command line based on the docopts string specification, load
        # them into dictionary "opts"
        opts = docopt(doc)
        print("Command line options:")
        print(opts)
        print("")
    
        # If version was chosen, print it and exit
        if (opts["--version"]):
            print ("Version: " + __version__)
            sys.exit()

    '''
    s = inspect.cleandoc(s)
    f = os.open(os.fspath(f'{dir_pck}/{pck}.py'), os.O_RDWR|os.O_CREAT)
    os.write(f, bytes(s, 'utf-8'))
    os.close(f)
        
    #---------------------------------------------------------------------------
    # test_{pck}.py

    # pytest
    s = f'''
    # run the tests in this file with the command:
    #     pytest test_{pck}.py
    
    import pytest
    from {pck} import {pck}

    def test_upper():
        assert 'foo'.upper() == 'FOO'
    '''
    s = inspect.cleandoc(s) + '\n'

    # unittest
    s_u = f'''
    # run this with the command:
    #     python test_{pck}.py
    
    import unittest
    from {pck} import {pck}
    
    class TestMyPackage(unittest.TestCase):
    
        # methods called once at the beginning and end around all test cases in
        # this class
    
        @classmethod
        def setUpClass(cls):
            pass
    
    
        @classmethod
        def tearDownClass(cls):
            pass
    
    
        # methods run for every test case:
    
        def setUp(self):
            pass
    
        def tearDown(self):
            pass
    
    
        # test cases:
    
        def test_upper(self):
            self.assertEqual('foo'.upper(), 'FOO')
    
        def test_isupper(self):
            self.assertTrue('FOO'.isupper())
            self.assertFalse('Foo'.isupper())
    
        def test_split(self):
            s = 'hello world'
            self.assertEqual(s.split(), ['hello', 'world'])
            # check that s.split fails when the separator is not a string
            with self.assertRaises(TypeError):
                s.split(2)
    
    if __name__ == '__main__':
        unittest.main()
    '''
    s_u = inspect.cleandoc(s_u) + '\n'

    # select unittest or pytest
    if (opts["-u"]):
        s = s_u

    f = os.open(os.fspath(f'{dir_tst}/test_{pck}.py'), os.O_RDWR|os.O_CREAT)
    os.write(f, bytes(s, 'utf-8'))
    os.close(f)
