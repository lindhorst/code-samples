# Create a skeleton directory structure for development of a new Python package

Install this package following the method from 09.deployment/02.cli-with-options

Run it as

``` bash
cps <new-package>
```

This will create a directory structure containing a few standard files under

``` bash
./<new-package>
```
