from setuptools import setup

# read version
__version__ = open('create_package_skeleton/VERSION').readline().strip()

setup(
    name = 'create_package_skeleton',
    description = 'Run this to create a directory structure and a few standard files when beginning development of a new Python package',
    author = 'Paul Hodor',
    author_email = 'paul@aurynia.com',
    url='https://gitlab.com/paul.hodor/code-samples',
    license = 'see LICENSE file',
    version = __version__,
    packages = ['create_package_skeleton'],
    package_data = {'create_package_skeleton': ['LICENSE', 'VERSION']},
    install_requires = [
        'docopt == 0.6.2'
    ],
    entry_points = {
        'console_scripts': {
            'cps = create_package_skeleton.create_package_skeleton:cps'
        }
    }
)
