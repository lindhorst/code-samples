################################################################################
# Create a list

# with literals
l1 = []
l2 = [5, 'cucu', None, 7]

# list() constructor
#   list(<iterable>)
l3 = list(range(1,10,2))

# list comprehension
l4 = [x*x for x in range(1,11)]


################################################################################
# Access list items

l2[0] # 5
l2[1] # 'cucu'
l2[2] # 
l2[3] # 7
l2[4] # error
l2[-1] # 7
l2[-2] # 
l2[-3] # 'cucu'
l2[-4] # 5
l2[-5] # error


################################################################################
# slicing
#
#   mylist[<start>:<stop>:step>]

l5 = [5, 'cucu', [], {}, 7]

l5[:]     # copy of l5
l5[::]    # copy of l5
l5[1:3]   # ['cucu', []]
l5[1:4:2] # ['cucu', {}]
l5[:3]    # [5, 'cucu', []]
l5[2:]    # [[], {}, 7]
l5[::2]   # [5, [], 7]


################################################################################
# assignments
#
#   mylist[<index>] = new_value
#   mylist[<slice>] = <iterable>

l6 = l5[:] # [5, 'cucu', [], {}, 7]
l6[2] = 'bau' # [5, 'cucu', 'bau', {}, 7]

l7 = l5[:] # [5, 'cucu', [], {}, 7]
l7[1:5:2] = range(1,3) # [5, 1, [], 2, 7]


################################################################################
# length

len(l5) # 5


################################################################################
# methods

#-------------------------------------------------------------------------------
# append(self, object, /)
#     Append object to the end of the list.
l8 = l5[:] # [5, 'cucu', [], {}, 7]
l8.append(True) # [5, 'cucu', [], {}, 7, True]

#-------------------------------------------------------------------------------
# clear(self, /)
#     Remove all items from list.
l9 = l5[:] # [5, 'cucu', [], {}, 7]
l9.clear() # []

#-------------------------------------------------------------------------------
# copy(self, /)
#     Return a shallow copy of the list.
l10 = l5.copy() # [5, 'cucu', [], {}, 7]

#-------------------------------------------------------------------------------
# count(self, value, /)
#     Return number of occurrences of value.
l11 = ['a','b','c','b','c','d','c','c','d','e']
l11.count('a') # 1
l11.count('b') # 2
l11.count('c') # 4

#-------------------------------------------------------------------------------
# extend(self, iterable, /)
#     Extend list by appending elements from the iterable.
l12 = l5[:] # [5, 'cucu', [], {}, 7]
l12.extend(range(10,50,10)) # [5, 'cucu', [], {}, 7, 10, 20, 30, 40]

#-------------------------------------------------------------------------------
# index(self, value, start=0, stop=9223372036854775807, /)
#     Return first index of value.
#
#     Raises ValueError if the value is not present.
l5.index('cucu') # 1
l5.index('bau') # ValueError

#-------------------------------------------------------------------------------
# insert(self, index, object, /)
#     Insert object before index.
l13 = l5[:] # [5, 'cucu', [], {}, 7]
l13.insert(3, 10) # [5, 'cucu', [], 10, {}, 7]

#-------------------------------------------------------------------------------
# pop(self, index=-1, /)
#     Remove and return item at index (default last).
#
#     Raises IndexError if list is empty or index is out of range.
l14 = l5[:] # [5, 'cucu', [], {}, 7]
l14.pop()
# 7
l14
# [5, 'cucu', [], {}]

l15 = l5[:] # [5, 'cucu', [], {}, 7]
l15.pop(3)
# {}
l15
# [5, 'cucu', [], 7]

#-------------------------------------------------------------------------------
# remove(self, value, /)
#     Remove first occurrence of value.
#
#     Raises ValueError if the value is not present.
l16 = [10,20,30,10,20,30]
l16.remove(20) # [10, 30, 10, 20, 30]

#-------------------------------------------------------------------------------
# reverse(self, /)
#     Reverse *IN PLACE*.
l17 = l5[:] # [5, 'cucu', [], {}, 7]
l17.reverse() # [7, {}, [], 'cucu', 5]


#-------------------------------------------------------------------------------
# sort(self, /, *, key=None, reverse=False)
#     Sort the list in ascending order and return None.
#
#     The sort is in-place (i.e. the list itself is modified) and stable (i.e. the
#     order of two equal elements is maintained).
#
#     If a key function is given, apply it once to each list item and sort them,
#     ascending or descending, according to their function values.
#
#     The reverse flag can be set to sort in descending order.
l18 = [10,20,30,10,20,30]
l18.sort() # [10, 10, 20, 20, 30, 30]

l19 = [10,20,30,10,20,30]
l19.sort(reverse=True) # [30, 30, 20, 20, 10, 10]

l20 = [[5], 'cucu', [], [7,8,9]]
l20.sort(key=len) # [[], [5], [7, 8, 9], 'cucu']

