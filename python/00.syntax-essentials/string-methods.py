################################################################################
# str class
#
#   help(str)


#===============================================================================
# methods

# capitalize(self, /)
#     Return a capitalized version of the string.
#
#     More specifically, make the first character have upper case and the rest lower
#     case.
print('  1: ' + 'the cat. in the hat'.capitalize())
# The cat. in the hat

# casefold(self, /)
#     Return a version of the string suitable for caseless comparisons.
print('  2: ' + 'The Cat in the Hat'.casefold())
# the cat in the hat

# center(self, width, fillchar=' ', /)
#     Return a centered string of length width.
#
#     Padding is done using the specified fill character (default is a space).
print('  3: ' + 'ab'.center(10))
#      ab
print('  4: ' + 'abc'.center(10, '='))
# ===abc====

# count(...)
#     S.count(sub[, start[, end]]) -> int
#
#     Return the number of non-overlapping occurrences of substring sub in
#     string S[start:end].  Optional arguments start and end are
#     interpreted as in slice notation.
print('  5:', 'the cat in the hat'.count('he'))
# 2
print('  6:', 'abracadabracadabra'.count('abr', 1, 12))
# 1


# encode(self, /, encoding='utf-8', errors='strict')
#     Encode the string using the codec registered for encoding.
#
#     encoding
#       The encoding in which to encode the string.
#     errors
#       The error handling scheme to use for encoding errors.
#       The default is 'strict' meaning that encoding errors raise a
#       UnicodeEncodeError.  Other possible values are 'ignore', 'replace' and
#       'xmlcharrefreplace' as well as any other name registered with
#       codecs.register_error that can handle UnicodeEncodeErrors.
print('  7:', 'the cat in the hat'.encode())
# b'the cat in the hat'
print('  8:', 'the cat in the hat'.encode('utf-16'))
# b'\xff\xfet\x00h\x00e\x00 \x00c\x00a\x00t\x00 \x00i\x00n\x00 \x00t\x00h\x00e\x00 \x00h\x00a\x00t\x00'


# endswith(...)
#     S.endswith(suffix[, start[, end]]) -> bool
#
#     Return True if S ends with the specified suffix, False otherwise.
#     With optional start, test S beginning at that position.
#     With optional end, stop comparing S at that position.
#     suffix can also be a tuple of strings to try.
print('  9:', 'the cat in the hat'.endswith('the hat'))     # True
print(' 10:', 'the cat in the hat'.endswith(('cat', 'at'))) # True
print(' 10:', 'the cat in the hat'.endswith('cat'))         # False
print(' 10:', 'the cat in the hat'.endswith('cat', 0, 7))   # True


# expandtabs(self, /, tabsize=8)
#     Return a copy where all tab characters are expanded using spaces.
#
#     If tabsize is not given, a tab size of 8 characters is assumed.
print(' 11:', 'the\tcat\tin\tthe\that'.expandtabs())
# the     cat     in      the     hat
print(' 12:', 'the\tcat\tin\tthe\that'.expandtabs(5))
# the  cat  in   the  hat


# find(...)
#     S.find(sub[, start[, end]]) -> int
#
#     Return the lowest index in S where substring sub is found,
#     such that sub is contained within S[start:end].  Optional
#     arguments start and end are interpreted as in slice notation.
#
#     Return -1 on failure.
print(' 13:', 'the cat in the hat'.find('at'))
# 5
print(' 14:', 'the cat in the hat'.find('at', 6))
# 16
print(' 15:', 'the cat in the hat'.find('bt'))
# -1


# format(...)
#     S.format(*args, **kwargs) -> str
#
#     Return a formatted version of S, using substitutions from args and kwargs.
#     The substitutions are identified by braces ('{' and '}').


# format_map(...)
#     S.format_map(mapping) -> str
#
#     Return a formatted version of S, using substitutions from mapping.
#     The substitutions are identified by braces ('{' and '}').


# index(...)
#     S.index(sub[, start[, end]]) -> int
#
#     Return the lowest index in S where substring sub is found,
#     such that sub is contained within S[start:end].  Optional
#     arguments start and end are interpreted as in slice notation.
#
#     Raises ValueError when the substring is not found.
print(' 16:', 'the cat in the hat'.index('at'))
# 5
print(' 17:', 'the cat in the hat'.index('at', 6))
# 16
#print(' 18:', 'the cat in the hat'.index('bt'))
# ValueError


# isalnum(self, /)
#     Return True if the string is an alpha-numeric string, False otherwise.
#
#     A string is alpha-numeric if all characters in the string are alpha-numeric and
#     there is at least one character in the string.
print(' 19:', 'The cat in the 7th hat'.isalnum()) # False
print(' 20:', 'Abracadabra7'.isalnum())           # True


# isalpha(self, /)
#     Return True if the string is an alphabetic string, False otherwise.
#
#     A string is alphabetic if all characters in the string are alphabetic and there
#     is at least one character in the string.
print(' 21:', 'Abracadabra7'.isalpha()) # False
print(' 22:', 'Abracadabra'.isalpha())  # True


# isascii(self, /)
#     Return True if all characters in the string are ASCII, False otherwise.
#
#     ASCII characters have code points in the range U+0000-U+007F.
#     Empty string is ASCII too.
print(' 23:', 'the cat in the hat'.isascii()) # True
print(' 24:', 'Lindström'.isascii())          # False
print(' 25:', '\u017F'.isascii())             # False


# isdecimal(self, /)
#     Return True if the string is a decimal string, False otherwise.
#
#     A string is a decimal string if all characters in the string are decimal and
#     there is at least one character in the string.
print(' 26:', '001'.isdecimal())    # True
print(' 27:', '3.14'.isdecimal())   # False
print(' 28:', '²'.isdecimal())      # False
print(' 29:', '¾'.isdecimal())      # False


# isdigit(self, /)
#     Return True if the string is a digit string, False otherwise.
#
#     A string is a digit string if all characters in the string are digits and there
#     is at least one character in the string.
print(' 30:', '001'.isdigit())    # True
print(' 31:', '3.14'.isdigit())   # False
print(' 32:', '²'.isdigit())      # True
print(' 33:', '¾'.isdigit())      # False


# isidentifier(self, /)
#     Return True if the string is a valid Python identifier, False otherwise.
#
#     Call keyword.iskeyword(s) to test whether string s is a reserved identifier,
#     such as "def" or "class".
print(' 34:', '_mystuff'.isidentifier())  # True
print(' 35:', '_my-stuff'.isidentifier()) # False


# islower(self, /)
#     Return True if the string is a lowercase string, False otherwise.
#
#     A string is lowercase if all cased characters in the string are lowercase and
#     there is at least one cased character in the string.
print(' 36:', 'the cat, in the 7th hat.'.islower()) # True
print(' 37:', 'The cat in the hat'.islower())       # False


# isnumeric(self, /)
#     Return True if the string is a numeric string, False otherwise.
#
#     A string is numeric if all characters in the string are numeric and there is at
#     least one character in the string.
print(' 38:', '001'.isdigit())    # True
print(' 39:', '3.14'.isdigit())   # False
print(' 40:', '²'.isdigit())      # True
print(' 41:', '¾'.isnumeric())    # True


# isprintable(self, /)
#     Return True if the string is printable, False otherwise.
#
#     A string is printable if all of its characters are considered printable in
#     repr() or if it is empty.
print(' 42:', ''.isprintable())       # True
print(' 43:', '#1'.isprintable())     # True
print(' 44:', '\n\t\r'.isprintable()) # False


# isspace(self, /)
#     Return True if the string is a whitespace string, False otherwise.
#
#     A string is whitespace if all characters in the string are whitespace and there
#     is at least one character in the string.
print(' 45:', ''.isspace())         # False
print(' 46:', '#1'.isspace())       # False
print(' 47:', '\n\t\r  '.isspace()) # True


# istitle(self, /)
#     Return True if the string is a title-cased string, False otherwise.
#
#     In a title-cased string, upper- and title-case characters may only
#     follow uncased characters and lowercase characters only cased ones.
print(' 48:', 'The Cat In The Hat'.istitle()) # True
print(' 49:', 'The Cat In THE Hat'.istitle()) # False


# isupper(self, /)
#     Return True if the string is an uppercase string, False otherwise.
#
#     A string is uppercase if all cased characters in the string are uppercase and
#     there is at least one cased character in the string.
print(' 50:', 'THE CAT, IN THE 7TH HAT.'.isupper()) # True
print(' 51:', 'THE CAT iN THE HAT'.isupper())       # False


# join(self, iterable, /)
#     Concatenate any number of strings.
#
#     The string whose method is called is inserted in between each given string.
#     The result is returned as a new string.
#
#     Example: '.'.join(['ab', 'pq', 'rs']) -> 'ab.pq.rs'
print(' 52:', ' '.join(['the', 'cat', 'in', 'the', 'hat']))
# the cat in the hat
print(' 53:', '--'.join(['the', 'cat', 'in', 'the', 'hat']))
# the--cat--in--the--hat
print(' 54:', ''.join(['abra', 'cad', 'abra']))
# abracadabra


# ljust(self, width, fillchar=' ', /)
#     Return a left-justified string of length width.
#
#     Padding is done using the specified fill character (default is a space).
print(' 55: -', 'the cat'.ljust(12), '-')
# - the cat      -
print(' 56: -', 'the cat'.ljust(12, '='), '-')
# - the cat===== -


# lower(self, /)
#     Return a copy of the string converted to lowercase.
print(' 57:', 'The Cat In The Hat'.lower())
# the cat in the hat


# lstrip(self, chars=None, /)
#     Return a copy of the string with leading whitespace removed.
#
#     If chars is given and not None, remove characters in chars instead.
print(' 58:-', ' = =the cat= = '.lstrip(), '-')
# - = =the cat= =  -
print(' 59:-', ' = =the cat= = '.lstrip('= '), '-')
# - the cat= =  -


# partition(self, sep, /)
#     Partition the string into three parts using the given separator.
#
#     This will search for the separator in the string.  If the separator is found,
#     returns a 3-tuple containing the part before the separator, the separator
#     itself, and the part after it.
#
#     If the separator is not found, returns a 3-tuple containing the original string
#     and two empty strings.
print(' 60:', 'abracadabra'.partition('ab'))
# ('', 'ab', 'racadabra')
print(' 61:', 'abracadabra'.partition('br'))
# ('a', 'br', 'acadabra')
print(' 62:', 'abracadabra'.partition('bb'))
# ('abracadabra', '', '')


# removeprefix(self, prefix, /)
#     Return a str with the given prefix string removed if present.
#
#     If the string starts with the prefix string, return string[len(prefix):].
#     Otherwise, return a copy of the original string.
print(' 63:', 'the cat in the hat'.removeprefix('the cat '))
# in the hat
print(' 64:', 'the cat in the hat'.removeprefix('The cat '))
# the cat in the hat


# removesuffix(self, suffix, /)
#     Return a str with the given suffix string removed if present.
#
#     If the string ends with the suffix string and that suffix is not empty,
#     return string[:-len(suffix)]. Otherwise, return a copy of the original
#     string.
print(' 65:', 'the cat in the hat'.removesuffix(' in the hat'))
# the cat
print(' 66:', 'the cat in the hat'.removesuffix(' In the hat'))
# the cat in the hat


# replace(self, old, new, count=-1, /)
#     Return a copy with all occurrences of substring old replaced by new.
#
#       count
#         Maximum number of occurrences to replace.
#         -1 (the default value) means replace all occurrences.
#
#     If the optional argument count is given, only the first count occurrences are
#     replaced.
print(' 67:', 'abracadabra'.replace('br','BRRR'))
# aBRRRacadaBRRRa
print(' 68:', 'abracadabra'.replace('br','BRRR', 1))
# aBRRRacadabra
print(' 69:', 'abracadabra'.replace('bb','BRRR'))
# abracadabra


# rfind(...)
#     S.rfind(sub[, start[, end]]) -> int
#
#     Return the highest index in S where substring sub is found,
#     such that sub is contained within S[start:end].  Optional
#     arguments start and end are interpreted as in slice notation.
#
#     Return -1 on failure.
print(' 70:', 'the cat in the hat'.rfind('at'))
# 16
print(' 71:', 'the cat in the hat'.rfind('at', 0, 10))
# 5
print(' 72:', 'the cat in the hat'.rfind('bt'))
# -1


# rindex(...)
#     S.rindex(sub[, start[, end]]) -> int
#
#     Return the highest index in S where substring sub is found,
#     such that sub is contained within S[start:end].  Optional
#     arguments start and end are interpreted as in slice notation.
#
#     Raises ValueError when the substring is not found.
print(' 73:', 'the cat in the hat'.rindex('at'))
# 16
print(' 74:', 'the cat in the hat'.rindex('at', 0, 10))
# 5
#print(' 75:', 'the cat in the hat'.rindex('bt'))
# ValueError


# rjust(self, width, fillchar=' ', /)
#     Return a right-justified string of length width.
#
#     Padding is done using the specified fill character (default is a space).
print(' 75: -', 'the cat'.rjust(12), '-')
# -      the cat -
print(' 76: -', 'the cat'.rjust(12, '='), '-')
# - =====the cat -


# rpartition(self, sep, /)
#     Partition the string into three parts using the given separator.
#
#     This will search for the separator in the string, starting at the end. If
#     the separator is found, returns a 3-tuple containing the part before the
#     separator, the separator itself, and the part after it.
#
#     If the separator is not found, returns a 3-tuple containing two empty strings
#     and the original string.
print(' 77:', 'abracadabra'.rpartition('ab'))
# ('abracad', 'ab', 'ra')
print(' 78:', 'abracadabra'.rpartition('br'))
# ('abracada', 'br', 'a')
print(' 79:', 'abracadabra'.rpartition('bb'))
# ('', '', 'abracadabra')


# rsplit(self, /, sep=None, maxsplit=-1)
#     Return a list of the substrings in the string, using sep as the separator string.
#
#       sep
#         The separator used to split the string.
#
#         When set to None (the default value), will split on any whitespace
#         character (including \n \r \t \f and spaces) and will discard
#         empty strings from the result.
#       maxsplit
#         Maximum number of splits.
#         -1 (the default value) means no limit.
#
#     Splitting starts at the end of the string and works to the front.
print(' 80:', 'the   cat in the   hat'.rsplit())
# ['the', 'cat', 'in', 'the', 'hat']
print(' 81:', 'the   cat in the   hat'.rsplit(maxsplit = 2))
# ['the   cat in', 'the', 'hat']


# rstrip(self, chars=None, /)
#     Return a copy of the string with trailing whitespace removed.
#
#     If chars is given and not None, remove characters in chars instead.
print(' 82:-', ' = =the cat= = '.rstrip(), '-')
# -  = =the cat= = -
print(' 83:-', ' = =the cat= = '.rstrip('= '), '-')
# -  = =the cat -


# split(self, /, sep=None, maxsplit=-1)
#     Return a list of the substrings in the string, using sep as the separator string.
#
#       sep
#         The separator used to split the string.
#
#         When set to None (the default value), will split on any whitespace
#         character (including \n \r \t \f and spaces) and will discard
#         empty strings from the result.
#       maxsplit
#         Maximum number of splits.
#         -1 (the default value) means no limit.
#
#     Splitting starts at the front of the string and works to the end.
#
#     Note, str.split() is mainly useful for data that has been intentionally
#     delimited.  With natural text that includes punctuation, consider using
#     the regular expression module.
print(' 84:', 'the   cat in the   hat'.split())
# ['the', 'cat', 'in', 'the', 'hat']
print(' 85:', 'the   cat in the   hat'.split(maxsplit = 2))
# ['the', 'cat', 'in the   hat']


# splitlines(self, /, keepends=False)
#     Return a list of the lines in the string, breaking at line boundaries.
#
#     Line breaks are not included in the resulting list unless keepends is given and
#     true.
print(' 86:', 'the cat\nin the hat\n'.splitlines())
# ['the cat', 'in the hat']
print(' 87:', 'the cat\nin the hat\n'.splitlines(True))
# ['the cat\n', 'in the hat\n']


# startswith(...)
#     S.startswith(prefix[, start[, end]]) -> bool
#
#     Return True if S starts with the specified prefix, False otherwise.
#     With optional start, test S beginning at that position.
#     With optional end, stop comparing S at that position.
#     prefix can also be a tuple of strings to try.
print(' 88:', 'the cat in the hat'.startswith('the cat in')) # True
print(' 89:', 'the cat in the hat'.startswith(' in the', 7)) # True


# strip(self, chars=None, /)
#     Return a copy of the string with leading and trailing whitespace removed.
#
#     If chars is given and not None, remove characters in chars instead.
print(' 90:-', ' = =the cat= = '.strip(), '-')
# - = =the cat= = -
print(' 91:-', ' = =the cat= = '.strip('= '), '-')
# - the cat -


# swapcase(self, /)
#     Convert uppercase characters to lowercase and lowercase characters to uppercase.
print(' 92:', 'ThE cAt, In ThE hAt.'.swapcase())
# tHe CaT, iN tHe HaT.


# title(self, /)
#     Return a version of the string where each word is titlecased.
#
#     More specifically, words start with uppercased characters and all remaining
#     cased characters have lower case.
print(' 93:', 'ThE cAt, In ThE hAt.'.title())
# The Cat, In The Hat.


# translate(self, table, /)
#     Replace each character in the string using the given translation table.
#
#       table
#         Translation table, which must be a mapping of Unicode ordinals to
#         Unicode ordinals, strings, or None.
#
#     The table must implement lookup/indexing via __getitem__, for instance a
#     dictionary or list.  If this operation raises LookupError, the character is
#     left untouched.  Characters mapped to None are deleted.
print(' 94:', 'the cat in the hat'.translate(str.maketrans('at','oT')))
# The coT in The hoT


# upper(self, /)
#     Return a copy of the string converted to uppercase.
print(' 95:', 'ThE cAt, In ThE hAt.'.upper())


# zfill(self, width, /)
#     Pad a numeric string with zeros on the left, to fill a field of the given width.
#
#     The string is never truncated.
print(' 96:', '123.45'.zfill(12))
# 000000123.45
print(' 97:', 'the cat'.zfill(12))
# 00000the cat
print(' 98:', 'the cat in the hat'.zfill(12))
# the cat in the hat


#===============================================================================
# static methods

# maketrans(...)
#     Return a translation table usable for str.translate().
#
#     If there is only one argument, it must be a dictionary mapping Unicode
#     ordinals (integers) or characters to Unicode ordinals, strings or None.
#     Character keys will be then converted to ordinals.
#     If there are two arguments, they must be strings of equal length, and
#     in the resulting dictionary, each character in x will be mapped to the
#     character at the same position in y. If there is a third argument, it
#     must be a string, whose characters will be mapped to None in the result.
print(' 99:', str.maketrans('at', 'oT', 'bdfi'))
# {97: 111, 116: 84, 98: None, 100: None, 102: None, 105: None}
print('100:', str.maketrans({'a':'o', 't':'T', 'b':None, 'd':None, 'f':None,
                             'i':None}))
# {97: 'o', 116: 'T', 98: None, 100: None, 102: None, 105: None}
print('101:', 'the cat in the hat'.translate(str.maketrans(
    {'a':'o', 't':'T', 'b':None, 'd':None, 'f':None, 'i':None})))
# The coT n The hoT

