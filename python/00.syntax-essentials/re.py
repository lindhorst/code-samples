################################################################################
# working with regular expressions

import re

s1 = 'The cat in the hat'


#===============================================================================
# functions


#-------------------------------------------------------------------------------
# compile(pattern, flags=0)
#     Compile a regular expression pattern, returning a Pattern object.
p = re.compile('(.)at')
print(' 1:', p.sub('\\1op', s1))
# The cop in the hop

p = re.compile('.at')
print(' 2:', p.sub('rat', s1, count=1))
# The rat in the hat

p = re.compile('the', flags=re.I)
print(' 3:', p.sub('ze', s1))
# ze cat in ze hat


#-------------------------------------------------------------------------------
# escape(pattern)
#     Escape special characters in a string.
print(' 4:', re.escape('abc*.[]$^'))
# abc\*\.\[\]\$\^


#-------------------------------------------------------------------------------
# findall(pattern, string, flags=0)
#     Return a list of all non-overlapping matches in the string.
# 
#     If one or more capturing groups are present in the pattern, return
#     a list of groups; this will be a list of tuples if the pattern
#     has more than one group.
# 
#     Empty matches are included in the result.
print(' 5:', re.findall('.at', s1))
# ['cat', 'hat']
print(' 6:', re.findall('.he', s1))
# ['The', 'the']


#-------------------------------------------------------------------------------
# finditer(pattern, string, flags=0)
#     Return an iterator over all non-overlapping matches in the
#     string.  For each match, the iterator returns a Match object.
# 
#     Empty matches are included in the result.
print(' 7:', [i for i in re.finditer('.at', s1)])
# [<re.Match object; span=(4, 7), match='cat'>, <re.Match object; span=(15, 18), match='hat'>]
print(' 8:', [i for i in re.finditer('.he', s1)])
# [<re.Match object; span=(0, 3), match='The'>, <re.Match object; span=(11, 14), match='the'>]


#-------------------------------------------------------------------------------
# fullmatch(pattern, string, flags=0)
#     Try to apply the pattern to all of the string, returning
#     a Match object, or None if no match was found.
print(' 9:', re.fullmatch('.at.*', s1))
# None
print('10:', re.fullmatch('.he.*', s1))
# <re.Match object; span=(0, 18), match='The cat in the hat'>


#-------------------------------------------------------------------------------
# match(pattern, string, flags=0)
#     Try to apply the pattern at the start of the string, returning
#     a Match object, or None if no match was found.
print('11:', re.match('.at', s1))
# None
print('12:', re.match('.he', s1))
# <re.Match object; span=(0, 3), match='The'>


#-------------------------------------------------------------------------------
# purge()
#     Clear the regular expression caches


#-------------------------------------------------------------------------------
# search(pattern, string, flags=0)
#     Scan through string looking for a match to the pattern, returning
#     a Match object, or None if no match was found.
print('13:', re.search('.at', s1))
# <re.Match object; span=(4, 7), match='cat'>
print('14:', re.search('.he', s1))
# <re.Match object; span=(0, 3), match='The'>


#-------------------------------------------------------------------------------
# split(pattern, string, maxsplit=0, flags=0)
#     Split the source string by the occurrences of the pattern,
#     returning a list containing the resulting substrings.  If
#     capturing parentheses are used in pattern, then the text of all
#     groups in the pattern are also returned as part of the resulting
#     list.  If maxsplit is nonzero, at most maxsplit splits occur,
#     and the remainder of the string is returned as the final element
#     of the list.
print('15:', re.split('.at', s1))
# ['The ', ' in the ', '']
print('16:', re.split('(.at)', s1))
# ['The ', 'cat', ' in the ', 'hat', '']


#-------------------------------------------------------------------------------
# sub(pattern, repl, string, count=0, flags=0)
#     Return the string obtained by replacing the leftmost
#     non-overlapping occurrences of the pattern in string by the
#     replacement repl.  repl can be either a string or a callable;
#     if a string, backslash escapes in it are processed.  If it is
#     a callable, it's passed the Match object and must return
#     a replacement string to be used.
print('17:', re.sub('(.)at', '\\1op', s1))
# The cop in the hop
print('18:', re.sub('.at', 'rat', s1, count=1))
# The rat in the hat
print('19:', re.sub('the', 'ze', s1, flags=re.I))
# ze cat in ze hat


#-------------------------------------------------------------------------------
# subn(pattern, repl, string, count=0, flags=0)
#     Return a 2-tuple containing (new_string, number).
#     new_string is the string obtained by replacing the leftmost
#     non-overlapping occurrences of the pattern in the source
#     string by the replacement repl.  number is the number of
#     substitutions that were made. repl can be either a string or a
#     callable; if a string, backslash escapes in it are processed.
#     If it is a callable, it's passed the Match object and must
#     return a replacement string to be used.
print('20:', re.subn('(.)at', '\\1op', s1))
# ('The cop in the hop', 2)
print('21:', re.subn('.at', 'rat', s1, count=1))
# ('The rat in the hat', 1)
print('22:', re.subn('the', 'ze', s1, flags=re.I))
# ('ze cat in ze hat', 2)
