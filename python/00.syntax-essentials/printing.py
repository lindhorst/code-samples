################################################################################
# print() function
#
#   print(*args, sep=' ', end='\n', file=None, flush=False)


print('Hello', "world")
# Hello world

print('Hello', "world", sep='+++')
# Hello+++world

print('Hello', "world", end='--')
# Hello world--(no newline)


################################################################################
# f-strings
#
#   f'... {<some expression>} ...'

h = 24
d = 7

print(f'''There are {d} days in a week and {h} hours in a day.
Therefore there are {d*h} hours in a week.''')
# There are 7 days in a week and 24 hours in a day.
# Therefore there are 168 hours in a week.

print(f'''{1:2d} {2:3d} {3:5d}
{1:2d} {20:3d} {300:5d}
{10:2d} {200:3d} {30000:5d}''')
#  1   2     3
#  1  20   300
# 10 200 30000

print(f'pi and e are approximately {3.14159:4.2f} and {2.71828:4.2f}.')
# pi and e are approximately 3.14 and 2.72.


################################################################################
# format() method

print('pi and e are approximately {} and {}.'.format(3.14, 2.72))
print('pi and e are approximately {1} and {0}.'.format(2.72, 3.14))
print('pi and e are approximately {pi} and {e}.'.format(e=2.72, pi=3.14))
print('pi and e are approximately {:4.2f} and {:4.2f}.'.format(3.14159, 2.71828))
print('pi and e are approximately {pi:4.2f} and {e:4.2f}.'.
      format(e=2.71828, pi=3.14159))
# pi and e are approximately 3.14 and 2.72.


################################################################################
# formatting with % - old style
#
#   'string' % values

print('pi and e are approximately %4.2f and %4.2f.' % (3.14159, 2.71828))
# pi and e are approximately 3.14 and 2.72.


################################################################################
# print to a file
#
# Use the open() function
#
#   open(file, mode='r', buffering=-1, encoding=None, errors=None,
#        newline=None, closefd=True, opener=None)
#
# Open modes:
#
#    ========= ===============================================================
#    Character Meaning
#    --------- ---------------------------------------------------------------
#    'r'       open for reading (default)
#    'w'       open for writing, truncating the file first
#    'x'       create a new file and open it for writing
#    'a'       open for writing, appending to the end of the file if it exists
#    'b'       binary mode
#    't'       text mode (default)
#    '+'       open a disk file for updating (reading and writing)
#    ========= ===============================================================

with open('tmp.txt', mode='w') as fl:
    print('Hello world', file=fl)


################################################################################
# pretty print with pprint
#
#   pprint(object, stream=None, indent=1, width=80, depth=None, *,
#          compact=False, sort_dicts=True, underscore_numbers=False)

d = {'Pennsylvania':'PA', 'New York':'NY'}

print(d)
# {'Pennsylvania': 'PA', 'New York': 'NY'}

from pprint import pprint
pprint(d, width=30)
# {'New York': 'NY',
#  'Pennsylvania': 'PA'}

