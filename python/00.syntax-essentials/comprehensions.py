################################################################################
# list
#
# syntax:
#   out = [<expression> for <var> in <collection> if <condition on var>]
#
# equivalent to:
#   for <var> in <collection>:
#       if <condition on var>:
#           out.append(<expression>)

# make list of squares of even numbers
sq = [x*x for x in range(1, 11) if x % 2 == 0]
# [4, 16, 36, 64, 100]


################################################################################
# dictionary
#
# syntax:
#   out = {<key>:<value> for ...}

# make dictionary of squares of even numbers
sq = {x:x*x for x in range(1, 11) if x % 2 == 0}
# {2: 4, 4: 16, 6: 36, 8: 64, 10: 100}


################################################################################
# set
#
# syntax:
#   out = {<element> for ...}

# letter set for a sentence
import string
sentence = 'Hello world'
ls = {ch.lower() for ch in sentence if ch in string.ascii_letters}
# {'d', 'o', 'l', 'w', 'h', 'r', 'e'}


