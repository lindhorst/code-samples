# Example of how to work with a Python virtual environment

## Initial setup of a work environment

``` bash
python3 -m venv ./workenv
source ./workenv/bin/activate
pip install --upgrade pip setuptools

```

Test setup:
``` bash
pip list
which python
python --version
which pip
pip --version
```

Deactivate:
``` bash
deactivate
```

## Run a script

``` bash
source <path/to/venv/directory>/bin/activate
python <script>.py
<path/to>/<script>.py
deactivate
```

Example

``` bash
source ./workenv/bin/activate
./hello.py
deactivate
```
