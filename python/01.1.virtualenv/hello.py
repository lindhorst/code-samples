#!/usr/bin/env python

import sys

print("Python version:")
print(sys.version)
print("")
print("Executable:")
print(sys.executable)
