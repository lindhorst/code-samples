# `docopt` subcommands

This example shows how to write documentation for a command-line tool that has subcommands.

## Install

```bash
python3 -m venv workenv
source workenv/bin/activate
pip install --upgrade pip setuptools
pip install src/
```

Installation makes the following commands available:

* docsub
* comm1
* comm2

## View help pages

Help page for main command:

```bash
docsub -h
```

Help pages for subcommands:

```bash
docsub comm1 -h
docsub comm2 -h
```

Subcommands are also independent programs:

```bash
comm1 -h
comm2 -h
```
