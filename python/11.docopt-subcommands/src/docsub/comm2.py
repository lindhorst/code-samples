import sys
import os
from docopt import docopt
from pathlib import Path
import inspect

def comm2():
    # name of the executable
    (prdir, prog) = os.path.split(os.path.abspath(sys.argv[0]))

    # Write this docopts string following POSIX syntax to define the command
    # line options you want
    doc = f"""
     
    Program "{prog}"

    {prog} can be run on its own or as a subcommand of docsub.

    Usage:
      {prog} [-abc]
      {prog} -h | --help

    Options:
      -h, --help     Show this screen

    """
    doc = inspect.cleandoc(doc)

    opts = docopt(doc)
    print(f"{prog} options:")
    print(opts)
    print("")
