import sys
import os
from docopt import docopt
from pathlib import Path
import inspect
from subprocess import call

__version__ = open(str(Path(__file__).parent.joinpath('VERSION')))\
              .readline().strip()

# This is the main entry point for the CLI
def docsub():

    # name of the executable
    (prdir, prog) = os.path.split(os.path.abspath(sys.argv[0]))

    # Write this docopts string following POSIX syntax to define the command
    # line options you want
    doc = f"""
     
    Program "docsub"

    The description of "docsub" goes here

    Usage:
      {prog} [-g | --global] <subcommand> [<args>...]
      {prog} -v | --version
      {prog} -h | --help

    Options:
      -g, --global   An option that applies to all subcommands
      -v, --version  Show version
      -h, --help     Show this screen

    Available subcommands are:
      comm1   Does something
      comm2   Does something else

    Help on specific subcommands is available with:
      {prog} <subcommand> (-h | --help)
    """
    doc = inspect.cleandoc(doc)

    # Parse the command line based on the docopts string specification, load
    # them into dictionary "opts"
    opts = docopt(doc, version = "Version: " + __version__, options_first = True)
    print("Global options:")
    print(opts)
    print("")

    if opts['<subcommand>'] == 'comm1':
        exit(call([f'{prdir}/comm1'] + opts['<args>']))
    elif opts['<subcommand>'] == 'comm2':
        exit(call([f'{prdir}/comm2'] + opts['<args>']))
    else:
        print(f'ERROR: "{opts["<subcommand>"]}" is not a valid subcommand')
        exit(call([f'{sys.argv[0]}', '--help']))
