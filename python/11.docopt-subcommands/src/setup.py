from setuptools import setup

__version__ = open('docsub/VERSION').readline().strip()

setup(
    name = 'docsub',
    description = '',
    author = '',
    author_email = '',
    url = '',
    license = 'see LICENSE file',
    version = __version__,
    packages = ['docsub'],
    package_data = {'docsub': ['LICENSE', 'VERSION']},
    install_requires = [
        'docopt == 0.6.2'
    ],
    entry_points = {
        'console_scripts': {
            'docsub = docsub.docsub:docsub',
            'comm1 = docsub.comm1:comm1',
            'comm2 = docsub.comm2:comm2'
        }
    }
)
