import sys
import os
from pprint import pprint

# always print
print('\n=====\nMOD 2\n=====\n')
print(f'os.getcwd(): {os.getcwd()}')
print(f'__file__:    {__file__}')
print(f'__name__:    {__name__}')
print(f'sys.path:')
pprint(sys.path)
print()

# use imported code from mod1
from . import mod1

def fun2():
    print('mod2: calling function "mod1.fun1": ', end = '')
    mod1.fun1(mod1.my_global/6)

fun2()
print()
