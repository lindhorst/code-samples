import sys
import os
from pprint import pprint

# always print
print('\n=====\nMOD 1\n=====\n')
print(f'os.getcwd(): {os.getcwd()}')
print(f'__file__:    {__file__}')
print(f'__name__:    {__name__}')
print('sys.path:')
pprint(sys.path)
print()

# print out something

# global variable
my_global = 12

# function
def fun1(x):
    print(f'my value is: {x}')

print('mod1: ', end='')
fun1(1)
print()
