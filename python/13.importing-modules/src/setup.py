from setuptools import setup

setup(
    name = 'mypack',
    description = 'investigate importing of modules',
    version = 1.0,
    packages = ['mypack'],
    entry_points = {
        'console_scripts': {
            'runmod2 = mypack.mod_main:main'
        }
    }
)
