# Import modules from the same package

There is a package called `mypack` containing 3 modules:

* `mod1`
* `mod2`
* `mod_main`

`mod2` imports `mod1`. The last module demonstrates behavior of a command-line
tool that imports `mod2`, and thus indirectly `mod1` as well.

## Main lesson

From a module within a package, import other modules from the same package using
the formula:

```python
from . import other_module
```

Don't do this:

```python
# DON'T DO THIS
import other_module
```

## Installation

Work in the directory where the `README.md` file is.

```bash
python3 -m venv workenv
source workenv/bin/activate
pip install --upgrade pip setuptools
pip install src/
```

## Commands that work

```bash
python -m mypack.mod2
python -m mypack.mod_main
runmod2
```

```text
(workenv) MLRGSRJ73:13.importing-modules phodor$ python
>>> import mypack.mod2
>>> mypack.mod2.fun2()
```

```text
(workenv) MLRGSRJ73:13.importing-modules phodor$ python
>>> import mypack.mod_main
>>> mypack.mod_main.main()
```
