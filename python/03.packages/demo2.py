#!/usr/bin/env python

# import an entire package or module

# Importing a package does not make modules available by default. Only when they
# are imported in the __init__.py do they becaome availabe from here. SO the
# following will not work:
#
# import mypack
# print("global_var", mypack.mymod1.global_var)
# mypack.mymod1.say_hello()

# import a module
import mypack.mymod2
mypack.mymod2.hello_class.hello_1()

# import a module and rename it
import mypack.mymod1 as m1
print("global_var", m1.global_var)
m1.say_hello()
