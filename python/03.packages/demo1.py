#!/usr/bin/env python

## import using "from"

# import a module
from mypack import mymod1
print("global_var ", mymod1.global_var)

# import an object from a module
from mypack.mymod1 import say_hello
say_hello()

# import all objects from a module
from mypack.mymod2 import *
hello_class.hello_1()

# import an object from a module, rename it
from mypack.mymod2 import hello_class as hc
hc.hello_2()
