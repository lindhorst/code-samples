# Packages and modules

A module is a source file containing Python objects. The name of the files
equals the name of the module, e.g. a file called ```mymod.py``` implements a
module called ```mymod```.

A package is a directory containing related modules and possibly other
files. Python 2 required the presence of a (possibly empty) file called
```__init___.py```, which indicates that the directory is a package. This file
is not required in Python 3.

In this example there is a package called ```mypack```, which contains 2
modules: ```mymod1``` ```and mymod2```. Demo scripts show how objects from the
package can be used.

