from docopt import docopt
import ldap

## Variables specific to the OpenLDAP instance. Edit as needed.

# # location of OpenLDAP
# url = 'ldap://pplkdc01.analytics.loc'  # 172.31.208.20
# # distinguished name
# dn = 'uid=b_p_bdldap_s,ou=people,dc=analytics,dc=loc'
# # subtree to seach
# subtree = 'dc=analytics,dc=loc'

# location of OpenLDAP
url = 'ldap://192.168.56.104'
# distinguished name
dn = 'cn=ldapadmin,dc=anura,dc=amphibia'
# subtree to seach
subtree = 'dc=anura,dc=amphibia'



DOC = """Read and print contents of OpenLDAP database

This script connects to the BDP OpenLDAP and prints the entire contents of the
database. It is meant to demo how the 'ldap' package can be used toi connect to
an OpenLDAP database and interact with it.

Usage:
  readall -p <password>

Options:
  -p <password>, --password <password>  The password needed to log into OpenLDAP

Do not type the password on the command line. Instead use an environment
variable, in which the password was stored following the method described at
https://confluence/display/SDO/Manage+passwords+on+the+command+line

"""

# read command line options
args = docopt(DOC)

# establish a connection
connect = ldap.initialize(url)
# bind operation - needed first
connect.simple_bind_s(dn, args['--password'])

# do the search
ldap_result_id = connect.search(subtree, ldap.SCOPE_SUBTREE)

# extract the records
result_set = []
while 1:
    result_type, result_data = connect.result(ldap_result_id, 0)
    if (result_data == []):
        break
    else:
        ## if you are expecting multiple results you can append them
        ## otherwise you can just wait until the initial result and break out
        if result_type == ldap.RES_SEARCH_ENTRY:
            result_set.append(result_data)
print(result_set)
