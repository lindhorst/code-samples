# Example of connecting to an OpenLDAP database and reading content from it

This example connects to an OpenLDAP database specified in the ```readall.py```
script. To use another database, change the appropriate variable values
in ```readall.py```.

## Set up virtual environment

Note that the package name to be installed by pip is ```python-ldap```,
not ```ldap```. If the latter name is used, the script will not work.

``` bash
python3 -m venv ./workenv
source ./workenv/bin/activate
pip install --upgrade pip setuptools
pip install -r requirements.txt

```

## Run the script

Export the password for user b_p_bdldap_s into variable $mypass following the
method described at
https://confluence/display/SDO/Manage+passwords+on+the+command+line

``` bash
python readall.py -p $mypass
```

