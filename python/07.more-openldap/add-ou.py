from docopt import docopt
import ldap
import ldap.modlist as modlist


## Variables specific to the OpenLDAP instance. Edit as needed.

# location of OpenLDAP
url = 'ldap://192.168.56.104'
# distinguished name
dn = 'cn=ldapadmin,dc=anura,dc=amphibia'
# subtree to seach
subtree = 'dc=anura,dc=amphibia'



DOC = """
Add an organizational unit called "util" to an OpenLDAP database

Usage:
  add-ou -p <password>

Options:
  -p <password>, --password <password>  The password needed to log into OpenLDAP

Do not type the password on the command line. Instead use an environment
variable, in which the password was stored following the method described at
https://confluence/display/SDO/Manage+passwords+on+the+command+line

"""

#===============================================================================
# Connect to OpenLDAP

# read command line options
args = docopt(DOC)

# establish a connection
conn = ldap.initialize(url)
# bind operation - needed first
conn.simple_bind_s(dn, args['--password'])


#===============================================================================
# do the work

# build the object to add to the db, in this case organizational unit "util"
dn = "ou=util,dc=anura,dc=amphibia"
attr = {}
attr['objectClass'] = b'organizationalUnit'
attr['ou'] = b'util'

# convert the dictionary to a modlist
ldif = modlist.addModlist(attr)

# add the record
conn.add_s(dn, ldif)

# disconnect
conn.unbind_s()
