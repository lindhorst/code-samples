# More examples using OpenLDAP

## Set up virtual environment

Same as for 06:

``` bash
python3 -m venv ./workenv
source ./workenv/bin/activate
pip install --upgrade pip setuptools
pip install -r requirements.txt
```

## Add and delete an organizational unit called "util"

``` bash
python add-ou.py -p $openldappw
python delete-ou.py -p $openldappw
```

