from docopt import docopt
import ldap

## Variables specific to the OpenLDAP instance. Edit as needed.

# location of OpenLDAP
url = 'ldap://pplkdc01.analytics.loc'  # 172.31.208.20
# distinguished name
dn = 'uid=b_p_bdldap_s,ou=people,dc=analytics,dc=loc'
# subtree to seach
subtree = 'ou=groups,dc=analytics,dc=loc'


DOC = """Read and print groups from OpenLDAP database

Usage:
  read-groups -p <password>

Options:
  -p <password>, --password <password>  The password needed to log into OpenLDAP

Do not type the password on the command line. Instead use an environment
variable, in which the password was stored following the method described at
https://confluence/display/SDO/Manage+passwords+on+the+command+line

"""

# read command line options
args = docopt(DOC)

# establish a connection
conn = ldap.initialize(url)
# bind operation - needed first
conn.simple_bind_s(dn, args['--password'])

# do the search
ldap_result_id = conn.search(subtree, ldap.SCOPE_SUBTREE)


# print header
print("cn\tgidNumber\tdescription")

# extract the records
result_set = []
while 1:
    result_type, result_data = conn.result(ldap_result_id, 0)
    if (result_data == []):
        break
    else:
        ## if you are expecting multiple results you can append them
        ## otherwise you can just wait until the initial result and break out
        if result_type == ldap.RES_SEARCH_ENTRY:
            res = result_data[0][1]
            if b'posixGroup' in res['objectClass']  or \
               b'groupOfNames' in res['objectClass']:
                cn = res['cn'][0].decode('utf-8')
                gid = ''
                if 'gidNumber' in res:
                    gid = res['gidNumber'][0].decode('utf-8')
                desc = ''
                if 'description' in res:
                    desc = res['description'][0].decode('utf-8')
                print (f"{cn}\t{gid}\t{desc}")
