# Example of writing documentation using docstrings

Test the docstrings from a Python console. First, open a console with:
```bash
python3
```

Try displaying a few help messages:
```python
help(hello)
help(hello.say_hello)
help(hello.hello_class)
help(hello.hello_class.hello_2)
```
