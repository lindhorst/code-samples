"""A module-level docstring

Docstrings are text delimited by runs of 3 double quotes. They have to be placed
immediately following the object they refer to.

A module in Python is a file containing Python objects and having the ".py"
extension. The name of the file is the name of the module.

Because this particular docstring is placed at the beginning of the Python source file, it refers to the module defined here.

After loading the module with
  import hello
display this string with
  help(hello)

There is no standard on how to write docstrings, but there are a few common
styles. See for example the blog post at
https://stackoverflow.com/questions/3898572/what-is-the-standard-python-docstring-format
for the description of some popular styles.

"""

global_var = 25
""" A module-level variable

Docstrings cannot be set for variables, because they are bound to objects. In this case
  help(hello.global_var)
shows the help for class 'int', not this docstring.
"""

def say_hello(who="world"):
    """Module-level function

    Look at the docstring with
      help(hello.say_hello)
    """
    print ("hello " + who)


class hello_class:
    """Class docstring

    Show this with
      help(hello.hello_class)
    """

    def hello_1(who="world 1"):
        """Class function

        Prints another "hello" message.
        
        See this dosctring with:
          help(hello.hello_class.hello_1)
        """
        print ("hello " + who)

    def hello_2(who = "world 2"):
        """Example of one-liner docstring"""
        print ("hello " + who)
